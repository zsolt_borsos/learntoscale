//
//  Options.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 07/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Options.h"
#import "MainMenu.h"
#import "SoundEngine.h"
#import "Tuner.h"

@implementation Options{

    SKNode *_bgLayer;
    Button *_soundButton;
    BOOL _soundOn;
    SoundEngine *_soundEngine;
    Button *_bgMusicButton;

}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
         _soundEngine = [SoundEngine sharedSound];
        _soundOn = _soundEngine.soundOn;
        
        [self createMenuLayout];
        if (_soundOn) {
            [_soundButton changeTextTo:@"Input Sound Off"];
        }else {
            [_soundButton changeTextTo:@"Input Sound On"];
        }
        
        if (_soundEngine.bgPlaying) {
            [_bgMusicButton changeTextTo:@"Background Music Off"];
        }else {
            [_bgMusicButton changeTextTo:@"Background Music On"];
        }
        
    }
    return self;
}

-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"back"] == YES) {
        MainMenu *scene = [[MainMenu alloc]initWithSize:self.size];
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
    }
    
    //other button actions
    if ([name isEqualToString:@"addXp"] == YES) {
        [self.player addXp:1000];
    }
    
    if ([name isEqualToString:@"addPick"] == YES) {
        [self.player addPick:5];
    }
    
    if ([name isEqualToString:@"resetUser"] == YES) {
        self.player = [[Account alloc]init];
        //[self.player setName:@"Zsolt"];
        [self.player save];
    }
    
    if ([name isEqualToString:@"tuner"] == YES) {
    
        Tuner *scene = [[Tuner alloc]initWithSize:self.size];
        SKTransition *reveal = [SKTransition doorwayWithDuration:1];
        [self.view presentScene:scene transition:reveal];
    }
    
    if ([name isEqualToString:@"bgMusicButton"] == YES) {
        [_soundEngine triggerBgMusic];
        if (_soundEngine.bgPlaying) {
            [_bgMusicButton changeTextTo:@"Background Music Off"];
        }else {
            [_bgMusicButton changeTextTo:@"Background Music On"];
        }
        
    }
    
    
    if ([name isEqualToString:@"sound"] == YES) {
        
        [_soundEngine soundTrigger];
        
        if (_soundOn) {
            _soundOn = NO;
            [_soundButton changeTextTo:@"Input Sound On"];
        }else {
            _soundOn = YES;
            [_soundButton changeTextTo:@"Input Sound Off"];
        }
    }
    
    
    
}



-(void)createMenuLayout {
    
    _bgLayer = [[SKNode alloc]init];
    
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"CharacterSheet2.png"];
    background.size = CGSizeMake(self.size.width - 20, self.size.height - 20);
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    
    
    SKLabelNode *displayScaleName = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    displayScaleName.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    displayScaleName.fontColor = [SKColor blackColor];
    displayScaleName.fontSize = 30;
    displayScaleName.position = CGPointMake(self.size.width/2, self.size.height - 70);
    displayScaleName.text = @"Options";
    [_bgLayer addChild:displayScaleName];
    
    
    //tuner
    Button *tunerButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(280, 50) withText:@"Tuner" andFontSize:20 andName:@"tuner"];
    tunerButton.position = CGPointMake(self.size.width/2, self.size.height/2 + 150);
    tunerButton.userInteractionEnabled = YES;
    tunerButton.delegate = self;
    [_bgLayer addChild:tunerButton];
    
    
    _soundButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(280, 50) withText:@"Input Sound On" andFontSize:20 andName:@"sound"];
    _soundButton.position = CGPointMake(self.size.width/2, self.size.height/2 + 90);
    _soundButton.userInteractionEnabled = YES;
    _soundButton.delegate = self;
    [_bgLayer addChild:_soundButton];
    
    
    _bgMusicButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(280, 50) withText:@"Background Music On" andFontSize:20 andName:@"bgMusicButton"];
    _bgMusicButton.position = CGPointMake(self.size.width/2, self.size.height/2 + 30);
    _bgMusicButton.userInteractionEnabled = YES;
    _bgMusicButton.delegate = self;
    [_bgLayer addChild:_bgMusicButton];
    
    
    //debug buttons
    
    if (self.debugging) {
        Button *addPickButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(280, 50) withText:@"Add 5 Pick" andFontSize:20 andName:@"addPick"];
        addPickButton.position = CGPointMake(self.size.width/2, self.size.height/2 -75);
        addPickButton.userInteractionEnabled = YES;
        addPickButton.delegate = self;
        [_bgLayer addChild:addPickButton];
        
        Button *addXpButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(280, 50) withText:@"Add 1000 Xp" andFontSize:20 andName:@"addXp"];
        addXpButton.position = CGPointMake(self.size.width/2, self.size.height/2 -130);
        addXpButton.userInteractionEnabled = YES;
        addXpButton.delegate = self;
        [_bgLayer addChild:addXpButton];
        
        Button *resetUserButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(140, 40) withText:@"Reset User" andFontSize:20 andName:@"resetUser"];
        resetUserButton.position = CGPointMake(self.size.width - 90, self.size.height/2 -185);
        resetUserButton.userInteractionEnabled = YES;
        resetUserButton.delegate = self;
        [_bgLayer addChild:resetUserButton];
        
    }
    
    
    Button *backb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(100, 50) withText:@" <--Back" andFontSize:20 andName:@"back"];
    backb.position = CGPointMake(70, 50);
    backb.userInteractionEnabled = YES;
    backb.delegate = self;
    [_bgLayer addChild:backb];
    
    [self addChild: _bgLayer];
    
    
    
    
}






@end
