//
//  QuestStats.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 24/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "QuestStats.h"
#import "CharacterSheet.h"

@implementation QuestStats{
    SKNode *_bgLayer;
    NSDictionary *_details;
    BOOL _questDone;
    int _earnedXp;
    int _xpForQuest;
    int _hits;
    int _allNotes;
    int _highestCombo;
    int _reward;
    int _questType;
    int _bonusXp;
    int _speed;
    
    
}


- (instancetype)initWithSize:(CGSize)size andDetails:(NSDictionary *)details
{
    self = [super initWithSize:size];
    if (self) {
        _details = details;
        _questDone = [[details valueForKey:@"questdone"] boolValue];
        _earnedXp = [[details valueForKey:@"earnedxp"] intValue];
        _xpForQuest = [[details valueForKey:@"xpforquest"] intValue];
        _hits = [[details valueForKey:@"hits"] intValue];
        _highestCombo = [[details valueForKey:@"highestcombo"] intValue];
        _allNotes = [[details valueForKey:@"allnotes"] intValue];
        _reward = [[details valueForKey:@"reward"] intValue];
        _questType = [[details valueForKey:@"questtype"] intValue];
        _speed = [[details valueForKey:@"speed"] intValue];
        
        if (_allNotes == _hits) {
            _bonusXp = self.player.level * 25;
        }else{
            _bonusXp = 0;
        }
        
        [self saveLevelResults];
        
        [self createMenuLayout];
        
        if (_questDone) {
            [self createWinStats];
        }else{
            [self createFailStats];
        }


        
        
    }
    return self;
}


-(void)incrementQuestCounter {
    
    switch (_questType) {
        case 0:
            self.player.easyQuestsDone++;
            break;
        case 1:
            self.player.normalQuestsDone++;
            break;
        case 2:
            self.player.hardQuestsDone++;
            break;
        case 3:
            self.player.challengeQuestsDone++;
            break;
        default:
            NSLog(@"Something is wrong?");
            break;
    }
    [self.player save];
    
}


-(void)saveLevelResults {
    
    [self.player addXp:_earnedXp];
    if (_questDone) {
        [self incrementQuestCounter];
        [self.player addXp:_xpForQuest];
        [self.player addXp:_bonusXp];
    }
    if (_reward > 0) {
        [self.player addPick:_reward];
    }
    
    [self checkForQuestTriggers];
    
    [self.player save];
    
}

-(void)checkForQuestTriggers {
    
    
    if (((self.player.easyQuestsDone % 10) == 0) && (_speed == 60)) {
        self.player.normalQuestAvailable = YES;
    }
    
    if (((self.player.normalQuestsDone % 5) == 0) && (_speed == 72)) {
        self.player.hardQuestAvailable = YES;
        self.player.normalQuestAvailable = NO;
    }
    
    if (((self.player.hardQuestsDone % 3) == 0) && (_speed == 85)) {
        self.player.challangeQuestAvailable = YES;
        self.player.hardQuestAvailable = NO;
    }
    
    if (_speed == 72) {
        self.player.normalQuestAvailable = NO;
    }
    
    if (_speed == 85) {
        self.player.hardQuestAvailable = NO;
    }
    
    if (_speed == 100) {
        self.player.challangeQuestAvailable = NO;
    }
    
    
    [self.player save];
    
}

-(void)buttonPressedWithName:(NSString *)name {
    
    //redirect back to the character sheet by default
    
    CharacterSheet *scene = [[CharacterSheet alloc]initWithSize:self.size];
    SKTransition *transition = [SKTransition fadeWithColor:[SKColor whiteColor] duration:1];
    [self.view presentScene:scene transition:transition];
    
    
}

-(void)createWinStats {
    //assuming we did the quest when calling this
    SKLabelNode *questStatus = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    questStatus.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    questStatus.fontColor = [SKColor blackColor];
    questStatus.fontSize = 30;
    questStatus.position = CGPointMake(self.size.width/2, self.size.height - 120);
    questStatus.text = @"Well done!";
    [_bgLayer addChild:questStatus];
    
    SKLabelNode *xpLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    xpLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    xpLabel.fontColor = [SKColor blackColor];
    xpLabel.fontSize = 20;
    xpLabel.position = CGPointMake(self.size.width/2, self.size.height - 180);
    xpLabel.text = [NSString stringWithFormat: @"You get: %d + %d Xp", _earnedXp, _xpForQuest];
    [_bgLayer addChild:xpLabel];
    
    if (_bonusXp > 0) {
        SKLabelNode *bonusXpLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
        bonusXpLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        bonusXpLabel.fontColor = [SKColor blackColor];
        bonusXpLabel.fontSize = 20;
        bonusXpLabel.position = CGPointMake(self.size.width/2, self.size.height - 220);
        bonusXpLabel.text = [NSString stringWithFormat: @"You have mastered the level!"];
        [_bgLayer addChild:bonusXpLabel];
        
        SKLabelNode *bonusXpLabel2 = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
        bonusXpLabel2.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        bonusXpLabel2.fontColor = [SKColor blackColor];
        bonusXpLabel2.fontSize = 20;
        bonusXpLabel2.position = CGPointMake(self.size.width/2, self.size.height - 260);
        bonusXpLabel2.text = [NSString stringWithFormat: @"You get an extra %d Xp!", _bonusXp];
        [_bgLayer addChild:bonusXpLabel2];
        
    }
    
    
    
    if (_reward > 0) {
        SKLabelNode *rewardLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
        rewardLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        rewardLabel.fontColor = [SKColor blackColor];
        rewardLabel.fontSize = 20;
        rewardLabel.position = CGPointMake(self.size.width/2, self.size.height - 300);
        rewardLabel.text = @"and you also get";
        [_bgLayer addChild:rewardLabel];
        
        SKLabelNode *rewardLabel2 = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
        rewardLabel2.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        rewardLabel2.fontColor = [SKColor blackColor];
        rewardLabel2.fontSize = 20;
        rewardLabel2.position = CGPointMake(self.size.width/2, self.size.height - 340);
        if (_reward > 1) {
            rewardLabel2.text = [NSString stringWithFormat:@"%d picks!", _reward];
        }else{
            rewardLabel2.text = [NSString stringWithFormat:@"%d pick!", _reward];
        }

        [_bgLayer addChild:rewardLabel2];
    }
    
    

}


-(void)createFailStats {
    SKLabelNode *questStatus = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    questStatus.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    questStatus.fontColor = [SKColor blackColor];
    questStatus.fontSize = 30;
    questStatus.position = CGPointMake(self.size.width/2, self.size.height - 120);
    questStatus.text = @"Quest failed...!";
    [_bgLayer addChild:questStatus];
    
    SKLabelNode *fillerText1 = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    fillerText1.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    fillerText1.fontColor = [SKColor blackColor];
    fillerText1.fontSize = 20;
    fillerText1.position = CGPointMake(self.size.width/2, self.size.height - 220);
    fillerText1.text = @"But you still get";
    [_bgLayer addChild:fillerText1];
    
    SKLabelNode *xpLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    xpLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    xpLabel.fontColor = [SKColor blackColor];
    xpLabel.fontSize = 20;
    xpLabel.position = CGPointMake(self.size.width/2, self.size.height - 260);
    xpLabel.text = [NSString stringWithFormat: @"your %d Xp!", _earnedXp];
    [_bgLayer addChild:xpLabel];
    
    SKLabelNode *rewardLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    rewardLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    rewardLabel.fontColor = [SKColor blackColor];
    rewardLabel.fontSize = 20;
    rewardLabel.position = CGPointMake(self.size.width/2, self.size.height - 300);
    rewardLabel.text = [NSString stringWithFormat:@"Try again sometime!"];
    [_bgLayer addChild:rewardLabel];
}

-(void)createMenuLayout {
    
    _bgLayer = [[SKNode alloc]init];
    
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor: [SKColor whiteColor] size:CGSizeMake(self.size.width - 20, self.size.height - 20)];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    
    /*
    SKLabelNode *displayTitle = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    displayTitle.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    displayTitle.fontColor = [SKColor blackColor];
    displayTitle.fontSize = 32;
    displayTitle.position = CGPointMake(self.size.width/2, self.size.height - 50);
    displayTitle.text = @"Your stats";
    [_bgLayer addChild:displayTitle];
    */
    
    
    Button *okbutton = [[Button alloc]initWithColor:[SKColor blueColor] size:CGSizeMake(250, 50) withText:@"OK" andFontSize:30 andName:@"okbutton"];
    okbutton.position = CGPointMake(self.size.width/2, 80);
    okbutton.userInteractionEnabled = YES;
    okbutton.delegate = self;
    [_bgLayer addChild:okbutton];
    
    
    [self addChild: _bgLayer];
    
    
    
    
}






@end
