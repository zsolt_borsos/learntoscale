//
//  SelectScale.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 07/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "SelectScale.h"
#import "Dashboard.h"

@implementation SelectScale{
    
    SKNode *_bgLayer;
    NSString *_currentScale;
    Button *_selectedScale;
    
}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        [self createMenuLayout];
        
    }
    return self;
}

-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"back"] == YES) {
        Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition flipVerticalWithDuration:0.4];
        [self.view presentScene:dash transition:transition];
        
    }else{
        
        /*
        if ([name isEqualToString:@"select"] == YES) {
            
            if (_selectedScale != nil) {
                //NSLog(@"current selection at button press: %@", _selectedScale.name);
                self.player.lastScale = _selectedScale.name;
                [self.player save];
                Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
                SKTransition *transition = [SKTransition doorsCloseVerticalWithDuration:0.5];
                [self.view presentScene:dash transition:transition];
            }
            
        }
        
        if (_selectedScale != nil) {
            _selectedScale.color = [SKColor darkGrayColor];
        }
        
        Button *b = (Button *)[_bgLayer childNodeWithName:name];
        _selectedScale = b;
        _selectedScale.color = [SKColor greenColor];
        //NSLog(@"current selection at selectiontime: %@", _selectedScale.description);
        */
        
        self.player.lastScale = name;
        [self.player save];
        Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsCloseVerticalWithDuration:0.5];
        [self.view presentScene:dash transition:transition];
        
    }
    
    
    NSLog(@"Button pressed in Select Scale Screen: %@", name);
}


-(void)createMenuLayout {
    
    _bgLayer = [[SKNode alloc]init];
    
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor: [SKColor whiteColor] size:CGSizeMake(self.size.width - 20, self.size.height - 20)];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    
    
    SKLabelNode *displayScaleName = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    displayScaleName.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    displayScaleName.fontColor = [SKColor blackColor];
    displayScaleName.fontSize = 30;
    displayScaleName.position = CGPointMake(self.size.width/2, self.size.height - 50);
    NSString *scaleNameStr = [NSString stringWithFormat:@"Level"];
    displayScaleName.text = scaleNameStr;
    [_bgLayer addChild:displayScaleName];
    
    
    /*
    Button *cb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(200, 50) withText:@"Select" andFontSize:35 andName:@"select"];
    cb.position = CGPointMake(self.size.width/2, 100);
    cb.userInteractionEnabled = YES;
    cb.delegate = self;
    [_bgLayer addChild:cb];
     
    */
    
    //NSString *path = [[NSBundle mainBundle] pathForResource:@"scales" ofType:@"plist"];
    //NSDictionary *scales = [NSDictionary dictionaryWithContentsOfFile:path];
   
    
    
    int rowPos = self.size.height - 100;
    int colPos = 90;
    int counter = 0;
    
    for (NSString *scale in self.player.levelsList) {
        if (counter == 2) {
            rowPos -= 45;
            colPos = 90;
            counter = 0;
        }
        Button *scaleButton = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(135, 40) withText:scale andFontSize:14 andName:scale];
        scaleButton.position = CGPointMake(colPos, rowPos);
        scaleButton.userInteractionEnabled = YES;
        scaleButton.delegate = self;
        [_bgLayer addChild:scaleButton];
      
        colPos += 140;

        counter++;
        //NSLog(@"Found levels: %@", scale.description);
    }
    
    Button *backb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(100, 50) withText:@" <--Back" andFontSize:20 andName:@"back"];
    backb.position = CGPointMake(65, 40);
    backb.userInteractionEnabled = YES;
    backb.delegate = self;
    [_bgLayer addChild:backb];
    
    [self addChild: _bgLayer];
    
    
    
    
}


@end
