//
//  LevelGenerator.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Level.h"

@interface LevelGenerator : NSObject

@property NSMutableArray *notes;
@property int difficulty;
//@property int rewardPicks;
@property(nonatomic, weak) NSString *scaleName;
@property NSArray *scaleNotes;
@property int complexity;
@property int scaleLength;
// 0 = C, 1 = C#, 2 = D etc...
@property int root;
@property int speed;


//This class is used to generate random levels from scales. Experimental!
//this currently only checks difficulty while creating levels.


//do not use this anymore, use the init below
-(id)initWithArrayOfNotesInScale:(NSArray *)scaleNotes onRoot:(int)root andName:(NSString *)scaleName andDifficulty:(int)difficulty andRewards:(int)picks;

//new init for levels
-(id)initWithRoot:(int)root andLevelName:(NSString *)levelName andDifficulty:(int)difficulty andComplexity:(int)complexity andLength:(int)length andSpeed:(int)speed;


-(void)generateChallengeLevel;
-(void)generateHardLevel;
-(void)generateNormalLevel;
-(Level *)generateEasyLevel;

-(void)generateNotes;

@end
