//
//  LtSScene.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LtSScene.h"

@implementation LtSScene{
    NSDate *_currentTime;
    //int _pickTime;
    //int _selectedPickTime;
}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        
        //change this for production
        self.debugging = YES;
        
        if ([self loadUserDetails] == NO) {
            NSLog(@"User details not found.");
        }
        
        /*
        
        //set pick time for player
        
        _selectedPickTime = 1800;
        
        if ([self loadUserDetails] == NO) {
            NSLog(@"User details not found.");
        }else {
            _currentTime = [NSDate date];
            _pickTime = _player.lastPickTime.timeIntervalSinceNow;
            //NSLog(@"Current user: %@", _player.name);
            //NSLog(@"Current time: %@", _currentTime);
            
            //NSLog(@"Last pick time: %@", _player.lastPickTime.description);
            //NSLog(@"Pick time: %d", _pickTime);
            if (_player.lastPickTime == 0) {
                _player.lastPickTime = [NSDate date];
                [_player save];
            }
        
            if (-(_pickTime) >= _selectedPickTime) {
                
                //set pick rate
                int picks = -(_pickTime) / _selectedPickTime;
                NSLog(@"Picks since last login: %i", picks);

                //add picks to player acc
                _player.picks += picks;
                
                
                //get remaining time
                int timeLeftover =  -(_pickTime) % _selectedPickTime;
                //NSLog(@"Time leftover: %i", timeLeftover);
                
                //set remaining time
                _player.lastPickTime = [NSDate dateWithTimeIntervalSinceNow:-timeLeftover];
                
                //save player
                [_player save];
                
                
            }
        }
        */
    }
    return self;
}


-(BOOL)loadUserDetails {
    
    Account *user = [[Account alloc]init];
    user = [user load];
    if (user.name.length > 0) {
        _player = user;
        return YES;
    }else {
        return NO;
    }
}

/*
-(int)getNextPickDueInTime {
    
    int timeForPick = [self.player.lastPickTime timeIntervalSinceNow];
    int formattedTime = (1800 + timeForPick) / 60;
    return formattedTime;
}

*/



@end
