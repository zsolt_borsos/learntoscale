//
//  LevelStatsScreen.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LevelStatsScreen.h"
#import "Dashboard.h"

@implementation LevelStatsScreen{
    SKNode *_bgLayer;
}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        
        [self createMenuLayout];
        
    }
    return self;
}

-(void)buttonPressedWithName:(NSString *)name {
    
    Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
    SKTransition *transition = [SKTransition fadeWithColor:[SKColor whiteColor] duration:0.5];
    [self.view presentScene:dash transition:transition];

    
}


-(void)createMenuLayout {
    
    _bgLayer = [[SKNode alloc]init];
    
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor: [SKColor whiteColor] size:CGSizeMake(self.size.width - 20, self.size.height - 20)];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    
    
    SKLabelNode *displayTitle = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    displayTitle.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    displayTitle.fontColor = [SKColor blackColor];
    displayTitle.fontSize = 30;
    displayTitle.position = CGPointMake(self.size.width/2, self.size.height - 50);
    displayTitle.text = @"Your stats";
    [_bgLayer addChild:displayTitle];
    
  
    
   // int rowPos = self.size.height - 100;
    
    
    

    Button *okbutton = [[Button alloc]initWithColor:[SKColor blueColor] size:CGSizeMake(250, 50) withText:@"OK" andFontSize:30 andName:@"okbutton"];
    okbutton.position = CGPointMake(self.size.width/2, 80);
    okbutton.userInteractionEnabled = YES;
    okbutton.delegate = self;
    [_bgLayer addChild:okbutton];
    
    
    [self addChild: _bgLayer];
    
    
    
    
}

    
    
    
@end
