//
//  LevelGenerator.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LevelGenerator.h"
#import "Note.h"

@implementation LevelGenerator{
    
    //strings
    int _startingString;
    int _currentString;
    int _lastString;
    int _jumpSize;
    BOOL _changeString;

    //runs
    int _runLength;
    BOOL _doRun;
    BOOL _inRun;
    BOOL _runUp;

    //notes
    int _startingNote;
    int _currentNotePos;
    int _lastNotePos;
    int _nextNotePos;
    
    //next note's direction
    BOOL _goUp;
    
    //plist
    NSDictionary *_scales;
}

//dont use this, use the one below!

-(id)initWithArrayOfNotesInScale:(NSArray *)scaleNotes onRoot:(int)root andName:(NSString *)scaleName andDifficulty:(int)difficulty andRewards:(int)picks {
   
    self = [super init];
    if (self) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"scales" ofType:@"plist"];
        _scales = [NSDictionary dictionaryWithContentsOfFile:path];
        
        
        _notes = [[NSMutableArray alloc]init];
        _scaleName = scaleName;
        _difficulty = difficulty;
        _scaleNotes = scaleNotes;
        //_rewardPicks = picks;
        _root = root;
        //lengths: ?? set lengths? maybe player selects?
        _scaleLength = 0;
        
        _goUp = [self getRandomYesNo];
        _doRun = NO;
        _inRun = NO;
        _runUp = YES;
        _lastNotePos = -1;
        _nextNotePos = -1;
        _currentNotePos = -1;
        _changeString = NO;
        
        [self setJumpSizeFromDifficulty];
       
        //[self generateNotes];
    }
    return self;
}

-(id)initWithRoot:(int)root andLevelName:(NSString *)levelName andDifficulty:(int)difficulty andComplexity:(int)complexity andLength:(int)length andSpeed:(int)speed {
    self = [super init];
    if (self) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"scales" ofType:@"plist"];
        _scales = [NSDictionary dictionaryWithContentsOfFile:path];
        
        _speed = speed;
        
        _notes = [[NSMutableArray alloc]init];
        _scaleName = levelName;
        _difficulty = difficulty;
        _scaleNotes = [_scales objectForKey:levelName];
        _root = root;
        _complexity = complexity;
        _scaleLength = length;
        
        _goUp = [self getRandomYesNo];
        _doRun = NO;
        _inRun = NO;
        _runUp = YES;
        _lastNotePos = -1;
        _nextNotePos = -1;
        _currentNotePos = -1;
        _changeString = NO;
        
        [self setJumpSizeFromDifficulty];
        
        
    }
    return self;
}


-(void)generateChallengeLevel {
    //[self generateNotes];
    
    
    
}


-(void)generateHardLevel {
    
    
}

-(void)generateNormalLevel {
    
    
}


-(Level *)generateEasyLevel {
    
    [self generateNewNotes];
    
    Level *l = [[Level alloc]init];
    l.notes = _notes;
    l.levelName = _scaleName;
    l.speed = 60;
    return l;
    
}

-(void)generateNotes {
    
    //1. get random starting point
    //2. start to walk the scale up or down
    //3. do random run
    //4. back to 2
    
    if (_scaleLength == 0) {
        _scaleLength = (_difficulty * 25) + 5;
    }
    
    _startingString = arc4random_uniform(5);
    _startingNote = arc4random_uniform(22);
    int pos = 0;
    int fretNumber = 0;
    _changeString = [self getRandomYesNo];
    
    
    for (int i = 0; i < _scaleLength; i++) {
        
        
        //!!!!!!!!!!!!!!!!!!!!!!!!!
        
        fretNumber = [self getNextRandomNoteInScale];
        
        //!!!!!!!!!!!!!!!!!!!!!!!!
        
        
        float midiNum = [self getMidiNumberForFretNumber:fretNumber onString:_currentString];
        
        Note *note = [[Note alloc]initWithString: (_currentString+1) andNumberOn: fretNumber andPosition: pos];
        note.midiNumber = midiNum;
        [_notes addObject:note];
        
        
        _lastNotePos = fretNumber;
        
         _lastString = _currentString;
        
        [self randomChangeString];
        
        //check if we changed string or not, we can adjust this to put notes on other strings way closer (chord like?)
        if (_currentString == _lastString) {
            
            /*
            calculation: 30 points for a note image to see it clearly
            worst case: 34 + 0 - 5 = 29 -> still ok !!!!!! this needs testing on max difficulty!!!!!!
            best case: 34+20-1 = 53 -> a bit of break for the player.
            */
            
            pos += (34 + arc4random_uniform(20))- _difficulty;
        }else {
            
            /*
             calculation: as we change strings we do not need to worry about visibility 
             (unless we change back?) !!!needs testing!!!!
             
             worst case: 25 + 0 - 15 = 20 -> play it double step like. Might be an issue if too big jumps are required?
             best case: 25+20-15 = 53 -> regular / easy change. A bit of break for the player again.
             */
             pos += (25 + arc4random_uniform(20))- (_difficulty * 3);
        }
       
    }
}


-(void)generateNewNotes {
    if (_scaleLength == 0) {
        _scaleLength = (_difficulty * 25) + 5;
    }
    
    _startingString = arc4random_uniform(5);
    _startingNote = arc4random_uniform(22);
    int pos = 0;
    int fretNumber;
    _changeString = [self getRandomYesNo];
    
    
    for (int i = 0; i < _scaleLength; i++) {
        
        
        //!!!!!!!!!!!!!!!!!!!!!!!!!
        
        fretNumber = [self getNextRandomNoteInScale];
        
        //!!!!!!!!!!!!!!!!!!!!!!!!
        
        
        float midiNum = [self getMidiNumberForFretNumber:fretNumber onString:_currentString];
        
        Note *note = [[Note alloc]initWithString: (_currentString+1) andNumberOn: fretNumber andPosition: pos];
        note.midiNumber = midiNum;
        [_notes addObject:note];
        
        
        _lastNotePos = fretNumber;
        
        _lastString = _currentString;
        
        [self randomChangeString];
        
        pos = arc4random_uniform(4) + 1;
        
    }
    
}

-(float)getMidiNumberForFretNumber:(int)fret onString:(int)string {
    int midiNumber;
    
    switch (string) {
        case 0:
            midiNumber = 64 + fret;
            break;
        case 1:
            midiNumber = 59 + fret;
            break;
        case 2:
            midiNumber = 55 + fret;
            break;
        case 3:
            midiNumber = 50 + fret;
            break;
        case 4:
            midiNumber = 45 + fret;
            break;
        case 5:
            midiNumber = 40 + fret;
            break;
        default:
            NSLog(@"Wrong string number??");
            break;
    }
    
    
    return midiNumber;
}

-(void)setJumpSizeFromDifficulty {
    
    switch (_difficulty) {
        case 1:
            _jumpSize = 0;
            break;
        case 2:
            _jumpSize = 1;
            break;
        case 3:
            _jumpSize = 2;
            break;
        case 4:
            _jumpSize = 3;
            break;
        case 5:
            _jumpSize = 4;
            break;
            
        default:
            _jumpSize = 1;
            break;
    }
    
    
}

-(void)randomChangeString {
    
    if (_changeString == YES) {
        
        int changeSize = arc4random_uniform(_jumpSize)+1;
        
        if ([self getRandomYesNo] == YES) {
            _currentString += changeSize;
        }else {
            _currentString -=changeSize;
        }
        
        //check for bounderies
        if (_currentString < 0) {
            _currentString = 0;
        }else if (_currentString > 4) {
            _currentString = 5;
        }
        
    }
    
     _changeString = [self getRandomYesNo];
    
}

-(int)getNextRandomNoteInScale {
    //flag to return
    BOOL foundNote = NO;
   
    //check bounderies
    _goUp = [self getNotFairRandomYesNo:_goUp];
    
    if (_lastNotePos == -1) {
        _currentString = _startingString;
        _currentNotePos = _startingNote;
    }
    
    NSArray *notesOnString = [NSArray arrayWithArray:[_scaleNotes objectAtIndex:_currentString]];
    while (foundNote == NO) {
        
        
        if (_lastNotePos == _currentNotePos) {
            if (_goUp == YES) {
                _currentNotePos++;
            }else {
                _currentNotePos--;
            }
        }else {
            if ([self getRandomYesNo] == YES) {
                if (_goUp == YES) {
                    _currentNotePos++;
                }else {
                    _currentNotePos--;
                }
            }
        }
        
        if (_currentNotePos < 0) {
            if (_lastNotePos == 0) {
                _currentNotePos = 1;
                _goUp = YES;
            }else {
                _currentNotePos += 2;
                _goUp = YES;
            }
        }
        
        if (_currentNotePos > 22) {
            if (_lastNotePos == 22) {
                _goUp = NO;
                _currentNotePos = 21;
            }else {
                _currentNotePos -= 2;
                _goUp = NO;
            }
        }
        foundNote = [[notesOnString objectAtIndex:_currentNotePos] boolValue];
        if (foundNote) {
            if (_currentNotePos < 0) {
                if (_lastNotePos == 0) {
                    _currentNotePos = 1;
                }else {
                    _currentNotePos += 2;
                    _goUp = YES;
                }
                foundNote = NO;
            }
            
            if (_currentNotePos > 22) {
                if (_lastNotePos == 22) {
                    _currentNotePos = 21;
                }else {
                    _currentNotePos -= 2;
                    _goUp = NO;
                }
                foundNote = NO;
            }
            if (foundNote == YES) {
                return _currentNotePos;
            }
            
            
        }else {
            if (_goUp == YES) {
                _currentNotePos++;
            }else {
                _currentNotePos--;
            }
            
        }
    }
    
    return -2;
}



-(BOOL)getNotFairRandomYesNo:(BOOL)previous {
    BOOL answer = !previous;
    
    int randomNumber = arc4random_uniform(100);
    if (randomNumber > 25) {
        return previous;
    }else {
        return answer;
    }
    
    
    return answer;
}

-(BOOL)getRandomYesNo {

    int randomNumber = arc4random_uniform(100);
    if (randomNumber > 50) {
        return YES;
    }else {
        return NO;
    }
}


@end
