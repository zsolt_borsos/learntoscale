//
//  PracticeGameplay.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 19/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "PracticeGameplay.h"
#import "LevelStatsScreen.h"
#import "Dashboard.h"

@implementation PracticeGameplay{
    
    
    //SKNode *self.menuLayer;
    SKLabelNode *_hitsLabel, *_diffLabel, *_scoreLabel, *_comboLabel, *_speedLabel, *_pitchLabel;
    
}



-(id)initWithSize:(CGSize)size andLevel:(Level *)level andSpeed:(float)speed {
    
    if (self = [super initWithSize:size andLevel:level andSpeed: speed]) {
        
        [self createMenuBarLayout];
        
    }
    return self;
}

-(void)doScore {
    
    self.hits++;
    self.score += 1;
    self.combo++;
    
}


-(void)addComboBonusToScore:(int)combo {
    
    //add animations here!
    if (combo > 2) {
        if (combo < 6) {
            self.score++;
        }else if (combo < 10) {
            self.score += 3;
        }else if (combo < 15) {
            self.score += 5;
        }else if (combo < 20) {
            self.score += 10;
        }else if (combo < 25) {
            self.score +=15;
        }else {
            self.score += 25;
        }
    }
    
    
}


-(void)updateTexts {
    
    _hitsLabel.text = [NSString stringWithFormat:@"Hits: %i", self.hits];
    _scoreLabel.text = [NSString stringWithFormat:@"Score: %i", self.score];
    if (self.gettingInput) {
        _pitchLabel.text = self.soundEngine.currentPitch;
    }
    _comboLabel.text = [NSString stringWithFormat:@"Combo: %i", self.combo];
    
}

-(void)levelEnded {
    
    SKTransition *transition = [SKTransition crossFadeWithDuration:0.5];
    //LevelStatsScreen *stats = [[LevelStatsScreen alloc]initWithSize:self.size];
    Dashboard *scene = [[Dashboard alloc]initWithSize:self.size];
    [self.view presentScene:scene transition:transition];
    
}



-(void)createMenuBarLayout {
    
    SKSpriteNode *menu = [SKSpriteNode spriteNodeWithImageNamed:@"menubar.png"];
    menu.anchorPoint = CGPointMake(1, 1);
    menu.yScale = 0.7;
    menu.position = CGPointMake(self.frame.size.width, self.frame.size.height);
    menu.name = @"menu";
    [self.menuLayer addChild:menu];
    
    
    //Left side
    
    _speedLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *speedStr = [NSString stringWithFormat:@"Speed: %1.0d", self.currentLevel.speed];
    _speedLabel.text = speedStr;
    _speedLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    _speedLabel.fontSize = 14;
    _speedLabel.position = CGPointMake(10, self.size.height -15);
    [self.menuLayer addChild:_speedLabel];
    
    
    _hitsLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *hitsStr = [NSString stringWithFormat:@"Hits: %i", self.hits];
    _hitsLabel.text = hitsStr;
    _hitsLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    _hitsLabel.fontSize = 14;
    _hitsLabel.position = CGPointMake(10, self.size.height -35);
    [self.menuLayer addChild:_hitsLabel];
    
    _comboLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *comboStr = [NSString stringWithFormat:@"Combo: %i", self.combo];
    _comboLabel.text = comboStr;
    _comboLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    _comboLabel.fontSize = 14;
    _comboLabel.position = CGPointMake(10, self.size.height -55);
    [self.menuLayer addChild:_comboLabel];
    
    
    //Middle for score
    
    _scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *scoreText = [NSString stringWithFormat:@"Score: %i", self.score];
    _scoreLabel.fontSize = 25;
    _scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    //_scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _scoreLabel.text = scoreText;
    _scoreLabel.position = CGPointMake(self.size.width/2, self.size.height - 40);
    [self.menuLayer addChild:_scoreLabel];
    
    _pitchLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    _pitchLabel.fontSize = 15;
    _pitchLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    //_pitchLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _pitchLabel.text = @"Pitch:";
    _pitchLabel.position = CGPointMake(self.size.width/2, self.size.height - 55);
    [self.menuLayer addChild:_pitchLabel];
    
    //right side
    
    
    SKLabelNode *level = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *levelStr = [NSString stringWithFormat:@"Level: %@", self.currentLevel.levelName];
    level.text = levelStr;
    level.fontSize = 14;
    //level.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    level.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    level.position = CGPointMake(self.size.width -10, self.size.height -15);
    [self.menuLayer addChild:level];
    
    _diffLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *diffStr = [NSString stringWithFormat:@"Diff: %i", self.currentLevel.difficulty];
    _diffLabel.text = diffStr;
    _diffLabel.fontSize = 14;
    _diffLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    _diffLabel.position = CGPointMake(self.size.width -10, self.size.height -35);
    [self.menuLayer addChild:_diffLabel];
    
    SKLabelNode *allnotes = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *allnotesStr = [NSString stringWithFormat:@"Notes: %lu", (unsigned long)[self.currentLevel.notes count]];
    allnotes.text = allnotesStr;
    allnotes.fontSize = 14;
    //allnotes.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    allnotes.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    allnotes.position = CGPointMake(self.size.width -10, self.size.height -55);
    [self.menuLayer addChild:allnotes];
    
    
    
}


@end
