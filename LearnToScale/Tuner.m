//
//  Tuner.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 10/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Tuner.h"
#import "MainMenu.h"

@implementation Tuner{
    
    SKNode *_bgLayer;
    
    SoundEngine *_soundEngine;
    SKLabelNode *_noteLabel, *_actualLabel;
    
}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        
        _soundEngine = [SoundEngine sharedSound];
    
        [self createMenuLayout];
        
    }
    return self;
}


-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"back"] == YES) {
       
        MainMenu *menu = [[MainMenu alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsCloseHorizontalWithDuration:0.5];
        [self.view presentScene:menu transition:transition];
    }
    
    //NSLog(@"Button pressed in Select Difficulty Screen: %@", name);
}


-(void)createMenuLayout {
    
    _bgLayer = [[SKNode alloc]init];
    
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor: [SKColor whiteColor] size:CGSizeMake(self.size.width - 20, self.size.height - 20)];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    
    
    SKLabelNode *displayScaleName = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    displayScaleName.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    displayScaleName.fontColor = [SKColor blackColor];
    displayScaleName.fontSize = 18;
    displayScaleName.position = CGPointMake(self.size.width/2, self.size.height - 50);
    displayScaleName.text = @"Tune your guitar";
    [_bgLayer addChild:displayScaleName];
    
    
    
    
    _noteLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    _noteLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _noteLabel.fontColor = [SKColor blackColor];
    _noteLabel.fontSize = 18;
    _noteLabel.position = CGPointMake(self.size.width/2, self.size.height - 90);
    _noteLabel.text = [NSString stringWithFormat:@"Note: %@", _soundEngine.currentPitch];
    [_bgLayer addChild:_noteLabel];
    
    
    _actualLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    _actualLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _actualLabel.fontColor = [SKColor blackColor];
    _actualLabel.fontSize = 18;
    _actualLabel.position = CGPointMake(self.size.width/2, self.size.height - 140);
    _actualLabel.text = [NSString stringWithFormat:@"Playing: %f", _soundEngine.currentPrecisePitch];
    [_bgLayer addChild:_actualLabel];
    
    Button *backb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(100, 50) withText:@" <--Back" andFontSize:20 andName:@"back"];
    backb.position = CGPointMake(65, 40);
    backb.userInteractionEnabled = YES;
    backb.delegate = self;
    [_bgLayer addChild:backb];
    
    
    [self addChild: _bgLayer];
    
    
}

-(void)update:(NSTimeInterval)currentTime {
    _noteLabel.text = [NSString stringWithFormat:@"Note: %@", _soundEngine.currentPitch];
     _actualLabel.text = [NSString stringWithFormat:@"Playing: %f", _soundEngine.currentPrecisePitch];
}

@end
