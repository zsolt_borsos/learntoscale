//
//  Quest.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 19/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Quest.h"

@implementation Quest

-(id)initWithRequirements:(NSArray *)requirements forLevel:(Level *)level andReward:(int)reward
{
    self = [super init];
    if (self) {
        self.requirements = requirements;
        self.level = level;
        self.reward = reward;
        self.xp = 0;
        self.type = level.type;
    }
    return self;
}

-(int)getRewardXpForQuest:(int)questTypeId {
    
    switch (questTypeId) {
        case 0:
            return 50;
            break;
        case 1:
            return 100;
            break;
        case 2:
            return 250;
            break;
        case 3:
            return 500;
            break;
        case 4:
            return 1000;
            break;
        default:
            return 50;
            break;
    }
    
    
}

-(int)getRewardPicksForQuest:(int)questTypeId {
    
    switch (questTypeId) {
        case 0:
            return 0;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return 3;
            break;
        case 3:
            return 5;
            break;
        case 4:
            return 8;
            break;
        default:
            return 1;
            break;
    }
    
}

@end
