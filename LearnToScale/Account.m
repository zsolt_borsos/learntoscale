//
//  Account.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 01/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Account.h"
#import "SoundEngine.h"

const float levelUpXpMultiplier = 1.25;
const float starterLevelXp = 1000;
const int starterPickLimit = 5;
const int pickLimitLevelUpValue = 5;
const NSArray *FIXEDUPGRADEPRICES;


@implementation Account{
    NSString *_docPath;
    SoundEngine *_soundEngine;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    //_achievements = [coder decodeObjectForKey:@"achievements"];
    //_lastAction = [coder decodeObjectForKey:@"lastAction"];
    
    _soundEngine = [SoundEngine sharedSound];
    
    //internal stuff
    _docPath = [coder decodeObjectForKey:@"docpath"];
    
    //game stuff
    
    _name = [coder decodeObjectForKey:@"name"];
    _activityLog = [coder decodeObjectForKey:@"activitylog"];
    
    //stats
    _level = [coder decodeIntForKey:@"level"];
    _exp = [coder decodeIntForKey:@"exp"];
    _pickLimit = [coder decodeIntForKey:@"picklimit"];
    
    //quests
    _easyQuestsDone = [coder decodeIntForKey:@"easyq"];
    _normalQuestsDone = [coder decodeIntForKey:@"normalq"];
    _hardQuestsDone = [coder decodeIntForKey:@"hardq"];
    _challengeQuestsDone = [coder decodeIntForKey:@"challengeq"];
    
    _normalQuestAvailable = [coder decodeBoolForKey:@"normalqrdy"];
    _hardQuestAvailable = [coder decodeBoolForKey:@"hardqrdy"];
    _challangeQuestAvailable = [coder decodeBoolForKey:@"challengeqrdy"];
    
    
    //upgrade levels
    _diffUpgradeLevel = [coder decodeIntForKey:@"diffupg"];
    _speedUpgradeLevel = [coder decodeIntForKey:@"speedupg"];
    _compUpgradeLevel = [coder decodeIntForKey:@"compupg"];
    _rootsList = [coder decodeObjectForKey:@"rootslist"];
    _levelsList = [coder decodeObjectForKey:@"levelslist"];
    
    
    //current stats for freeplay
    _picks = [coder decodeIntForKey:@"picks"];
    _lastScale = [coder decodeObjectForKey:@"lastscale"];
    _lastDiff = [coder decodeIntForKey:@"lastdiff"];
    _lastPickTime = [coder decodeObjectForKey:@"lastpicktime"];
    _lastSpeed = [coder decodeIntForKey:@"lastspeed"];
    _lastComplexity = [coder decodeObjectForKey:@"lastcomp"];
    _lastRoot = [coder decodeIntForKey:@"lastroot"];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    //[coder encodeObject:_achievements forKey:@"achievements"];
    //[coder encodeObject:_lastAction forKey:@"lastAction"];
    
    //internal stuff
    [coder encodeObject:_docPath forKey:@"docpath"];
    
    
    //game stuff
    [coder encodeObject:_name forKey:@"name"];
    [coder encodeObject:_activityLog forKey:@"activitylog"];
    
    //stats
    [coder encodeInt:_level forKey:@"level"];
    [coder encodeInt:_exp forKey:@"exp"];
    [coder encodeInt:_pickLimit forKey:@"picklimit"];
    
    //quests
    [coder encodeInt:_easyQuestsDone forKey:@"easyq"];
    [coder encodeInt:_normalQuestsDone forKey:@"normalq"];
    [coder encodeInt:_hardQuestsDone forKey:@"hardq"];
    [coder encodeInt:_challengeQuestsDone forKey:@"challengeq"];
    
    [coder encodeBool:_normalQuestAvailable forKey:@"normalqrdy"];
    [coder encodeBool:_hardQuestAvailable forKey:@"hardqrdy"];
    [coder encodeBool:_challangeQuestAvailable forKey:@"challengeqrdy"];
    
    //roots and levels/scales
    [coder encodeObject:_rootsList forKey:@"rootslist"];
    [coder encodeObject:_levelsList forKey:@"levelslist"];
    
    
    //upgrade levels
    [coder encodeInt:_diffUpgradeLevel forKey:@"diffupg"];
    [coder encodeInt:_speedUpgradeLevel forKey:@"speedupg"];
    [coder encodeInt:_compUpgradeLevel forKey:@"compupg"];
    
    //current stats
    [coder encodeInt:_picks forKey:@"picks"];
    [coder encodeObject:_lastScale forKey:@"lastscale"];
    [coder encodeInt:_lastDiff forKey:@"lastdiff"];
    [coder encodeObject:_lastPickTime forKey:@"lastpicktime"];
    [coder encodeInt:_lastSpeed forKey:@"lastspeed"];
    [coder encodeObject:_lastComplexity forKey:@"lastcomp"];
    [coder encodeInt:_lastRoot forKey:@"lastroot"];
    
    
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self setDocPath];
        NSString *path = [[NSBundle mainBundle] pathForResource:@"upgradePrices" ofType:@"plist"];
        FIXEDUPGRADEPRICES = [NSArray arrayWithContentsOfFile:path];
        
        //setting defaults for new user
        _activityLog = [[NSArray alloc]init];
        //char stats
        _level = 1;
        _exp = 0;
        _pickLimit = 5;
        //quest stats
        _easyQuestsDone = 0;
        _normalQuestsDone = 0;
        _hardQuestsDone = 0;
        _challengeQuestsDone = 0;
        
        _normalQuestAvailable = NO;
        _hardQuestAvailable = NO;
        _challangeQuestAvailable = NO;
        
        //add default root (C)
        _rootsList = [[NSMutableArray alloc]init];
        NSNumber *c = [NSNumber numberWithBool:YES];
        [_rootsList insertObject:c atIndex:0];
        
        
        //add default levels/scales
        _levelsList = [[NSMutableArray alloc]init];
        NSString *major = [NSString stringWithFormat:@"Major"];
        NSString *blues = [NSString stringWithFormat:@"Pentatonic Blues"];
        [_levelsList addObject:major];
        [_levelsList addObject:blues];
        
        //set starter levels
        _diffUpgradeLevel = 1;
        _speedUpgradeLevel = 1;
        _compUpgradeLevel = 1;
        
        _picks = 0;
        
    }
    return self;
}

-(Account *)load {
    
    return [NSKeyedUnarchiver unarchiveObjectWithFile: _docPath];
}

-(BOOL)save {
    
    /*
    if ([NSKeyedArchiver archiveRootObject:self toFile:_docPath] == NO) {
        NSLog(@"Failed to save user details!");
        NSString *msg = NSInvalidArchiveOperationException;
        NSLog(@"%@", msg);
        return NO;
    }
    return YES;
    */
    _docPath = [self getDocPath];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    if ([data writeToFile:_docPath atomically:YES] == NO) {
        NSLog(@"Failed to write.");
        NSString *msg = NSInvalidArchiveOperationException;
        NSLog(@"%@", msg);
        return NO;
    }else {
        return YES;
    }
    
    
    
}

-(NSString *)getDocPath {

    NSArray *docFolders =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                            NSUserDomainMask, YES);
    NSString *docFolderPath = [docFolders lastObject];
    return [docFolderPath stringByAppendingPathComponent:@"user.txt"];
}


-(void)setDocPath{
    NSArray *docFolders =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    NSString *docFolderPath = [docFolders lastObject];
    _docPath = [docFolderPath stringByAppendingPathComponent:@"user.txt"];
    //NSLog(@"Path: %@", _docPath);
}


-(int)nextLevelup {
    int nextLvl = starterLevelXp;
    
    if (_level == 1) {
        return nextLvl;
    }
    
    for (int i = 1; i <= _level; i++) {
        nextLvl = (nextLvl * 1.25) + nextLvl;
    }
    return nextLvl;
    
}


-(void)doLevelUp {
    
    //increase pick limit and level
    _pickLimit += pickLimitLevelUpValue;
    _level++;
    [self save];
    [_soundEngine playLevelUpSound];
    
}

-(BOOL)upgradeRoot:(int)rootId {
    
    //not sure how this will work out, but lets try! 0 = C, 1 = C# etc
    //all I need is BOOL value on 12 elements showing the status of the roots I own
    //default 0 = yes, rest is NO
    [_rootsList insertObject:[NSNumber numberWithBool:YES] atIndex:rootId];
    [self save];
    return YES;
    
}

-(BOOL)upgradeLevel:(NSString *)levelName {
    
    //might need some "error" checking here, maybe from plist for level existance??
    [_levelsList addObject:levelName];
    [self save];
    return YES;
    
}

-(BOOL)upgradeDiff {
    
    NSNumber *costToUpgrade = [FIXEDUPGRADEPRICES objectAtIndex:(_diffUpgradeLevel)];
    if (_picks >= [costToUpgrade intValue]) {
        _picks -= [costToUpgrade intValue];
        _diffUpgradeLevel++;
        [self save];
        return YES;
    }else {
        return FALSE;
    }
}


//upgrades speed level by 1 and removes the cost from the user
//costs are stored in plist (fixedupgradeprices)
-(BOOL)upgradeSpeed {
    
    NSNumber *costToUpgrade = [FIXEDUPGRADEPRICES objectAtIndex:(_speedUpgradeLevel)];
    //NSLog(@"cost: %@", costToUpgrade);
    if (_picks >= [costToUpgrade intValue]) {
        _picks -= [costToUpgrade intValue];
        _speedUpgradeLevel++;
        [self save];
        return YES;
    }else {
        return FALSE;
    }
}

-(BOOL)upgradeComp {
    NSNumber *costToUpgrade = [FIXEDUPGRADEPRICES objectAtIndex:(_speedUpgradeLevel)];
    if (_picks >= [costToUpgrade intValue]) {
        _picks -= [costToUpgrade intValue];
        _compUpgradeLevel++;
        [self save];
        return YES;
    }else {
        return FALSE;
    }
}

-(void)addPick:(int)p {
    
    if (_picks < _pickLimit) {
        _picks += p;
        if (_picks > _pickLimit) {
            _picks = _pickLimit;
        }
        [self save];
    }
    
}

-(void)removePick:(int)pickToRemove {
    _picks -= pickToRemove;
    if (_picks < 0) {
        _picks = 0;
    }
    [self save];
    
}

-(void)addXp:(int)xp {
    
    _exp += xp;
    if (_exp > [self nextLevelup]) {
        [self doLevelUp];
    }
    [self save];
    
}



@end
