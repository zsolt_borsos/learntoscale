//
//  SelectComplexity.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 11/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "SelectComplexity.h"
#import "Dashboard.h"

@implementation SelectComplexity{
    
    SKNode *_bgLayer;
    
}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        
        [self createMenuLayout];
        
    }
    return self;
}


-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"back"] == YES) {
        
        Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsCloseHorizontalWithDuration:0.5];
        [self.view presentScene:dash transition:transition];
    }else{
        
        self.player.lastComplexity = name;
        [self.player save];
        Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsCloseVerticalWithDuration:0.5];
        [self.view presentScene:dash transition:transition];
        
    }
    
    //NSLog(@"Button pressed in Select Difficulty Screen: %@", name);
}


-(void)createMenuLayout {
    
    
    _bgLayer = [[SKNode alloc]init];
    //background
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor: [SKColor whiteColor] size:CGSizeMake(self.size.width - 20, self.size.height - 20)];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    
    //title
    SKLabelNode *displayScaleName = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    displayScaleName.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    displayScaleName.fontColor = [SKColor blackColor];
    displayScaleName.fontSize = 30;
    displayScaleName.position = CGPointMake(self.size.width/2, self.size.height - 50);
    displayScaleName.text = @"Complexity";
    [_bgLayer addChild:displayScaleName];

    
    //buttons
    
    Button *fixedButton = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(240, 50) withText: @"Fixed" andFontSize:24 andName: @"Fixed"];
    fixedButton.position = CGPointMake(self.size.width/2, self.size.height - 100);
    fixedButton.userInteractionEnabled = YES;
    fixedButton.delegate = self;
    [_bgLayer addChild:fixedButton];
    
    if (self.player.compUpgradeLevel >= 2) {
        
        Button *changesButton = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(240, 50) withText: @"Changes" andFontSize:24 andName: @"Changes"];
        changesButton.position = CGPointMake(self.size.width/2, self.size.height - 160);
        changesButton.userInteractionEnabled = YES;
        changesButton.delegate = self;
        [_bgLayer addChild:changesButton];
    }
    
    if (self.player.compUpgradeLevel >= 3) {
        Button *randomButton = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(240, 50) withText: @"Random" andFontSize:24 andName: @"Random"];
        randomButton.position = CGPointMake(self.size.width/2, self.size.height - 220);
        randomButton.userInteractionEnabled = YES;
        randomButton.delegate = self;
        [_bgLayer addChild:randomButton];

    }
    
    
    
    
    Button *backb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(100, 50) withText:@" <--Back" andFontSize:20 andName:@"back"];
    backb.position = CGPointMake(65, 40);
    backb.userInteractionEnabled = YES;
    backb.delegate = self;
    [_bgLayer addChild:backb];
    
    
    [self addChild: _bgLayer];
    
}




@end
