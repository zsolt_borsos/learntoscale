//
//  LtSScene.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Account.h"


@interface LtSScene : SKScene <UIApplicationDelegate>

@property Account *player;
@property BOOL debugging;

//-(int)getNextPickDueInTime;

@end
