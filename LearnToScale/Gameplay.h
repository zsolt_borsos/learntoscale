//
//  Gameplay.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LtSScene.h"
#import "Level.h"
#import "Note.h"
#import "SoundEngine.h"

@protocol endLevelDelegate <NSObject>

-(void)levelEnded;

@end

@interface Gameplay : LtSScene <hitNoteDelegate, endLevelDelegate>

@property Level *currentLevel;

@property (nonatomic, weak)id<endLevelDelegate>levelDelegate;
//@property (nonatomic, weak)id<hitNoteDelegate>hitDelegate;

@property int score;
@property int hits;

@property BOOL mapFinished;
//AVAudioPlayer *_backgroundMusicPlayer;
@property SKNode *bgLayer;
@property SKNode *menuLayer;
@property NSTimeInterval lastUpdateTime;
@property NSTimeInterval dt;
@property CGPoint velocity;
@property CGPoint lastTouchLocation;
@property float speed;
@property int combo;
@property int highestCombo;

@property BOOL loadOnce;
@property int playerPosition;
@property SoundEngine *soundEngine;
//PdDispatcher *_dispatcher;
@property float currentPlayedPitch;

@property BOOL gettingInput;



-(id)initWithSize:(CGSize)size andLevel:(Level *)level andSpeed:(float)speed;
-(void)loadLevel:(Level *)level;

-(void)doScore;
-(void)updateTexts;
-(void)levelEnded;
-(void)createMenuBarLayout;
-(void)addComboBonusToScore;
-(void)resetCombos;

@end
