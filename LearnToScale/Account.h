//
//  Account.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 01/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Account : NSObject <NSCoding>


@property NSString *name;
@property NSArray *activityLog;

@property NSArray *FIXEDUPGRADEPRICES;


//stats
@property int level;
@property int exp;
@property int pickLimit;


//current quests status
//these will be reseted back to 0 once they reach the target to drive the gameflow
@property int easyQuestsDone;
@property int normalQuestsDone;
@property int hardQuestsDone;
@property int challengeQuestsDone;

@property BOOL normalQuestAvailable;
@property BOOL hardQuestAvailable;
@property BOOL challangeQuestAvailable;

//counters for sums to store the overall stats  //not in use, yet
@property int allEasyQuestsDone;
@property int allNormalQuestsDone;
@property int allHardQuestsDone;
@property int allChallengeQuestsDone;


//roots and levels/scales
@property NSMutableArray *rootsList;
@property NSMutableArray *levelsList;

//upgrade levels
@property int diffUpgradeLevel;
@property int speedUpgradeLevel;
@property int compUpgradeLevel;

//current stats
@property int picks;
@property NSString *lastScale;
@property int lastDiff;
@property NSDate *lastPickTime;
@property int lastSpeed;
@property NSString *lastComplexity;
// 0 = C, 1 = C#...
@property int lastRoot;




-(BOOL)save;
-(Account *)load;

-(int)nextLevelup;
-(void)doLevelUp;
-(BOOL)upgradeRoot:(int)rootId;
-(BOOL)upgradeLevel:(NSString *)levelName;


-(BOOL)upgradeDiff;


//upgrades speed level by 1 and removes the cost from the user
//costs are stored in plist (fixedupgradeprices)
-(BOOL)upgradeSpeed;


-(BOOL)upgradeComp;
-(void)addPick:(int)picks;
-(void)removePick:(int)pickToRemove;
-(void)addXp:(int)xp;



@end
