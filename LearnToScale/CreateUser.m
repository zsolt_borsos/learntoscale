//
//  CreateUser.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "CreateUser.h"
#import "MainMenu.h"





@implementation CreateUser{

    SKNode *_bgLayer;
    UITextField *_nameField;
}



- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        _bgLayer = [[SKNode alloc]init];
        [self addChild:_bgLayer];
        [self createLayout];
        
    }
    return self;
}

-(void)createLayout {
    
    SKSpriteNode *bg = [[SKSpriteNode alloc]initWithImageNamed:@"menubg.png"];
    [_bgLayer addChild:bg];
    
    
    
    SKLabelNode *label = [SKLabelNode labelNodeWithText:@"Enter your name"];
    label.fontName = @"Noteworthy-Bold";
    label.fontSize = 35;
    label.position = CGPointMake(self.size.width/2, self.size.height - 60);
    [_bgLayer addChild:label];
    
    Button *createUserButton = [[Button alloc]initWithColor:[SKColor blueColor] size:CGSizeMake(200, 50) withText:@"Create User" andFontSize:35 andName:@"createUser"];
    createUserButton.delegate = self;
    [createUserButton setUserInteractionEnabled:YES];
    createUserButton.position = CGPointMake(160, self.size.height - 120);
    [_bgLayer addChild:createUserButton];
    
    
}

-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"createUser"] == YES) {
        
        NSLog(@"Creating user: %@.", _nameField.text);
        
        if (_nameField.text.length == 0) {
            _nameField.placeholder = @"Enter your name...";
            return;
        }
        Account *player = [[Account alloc]init];
        
        //set default values
        [player setName:_nameField.text];
        
        if ([player save]) {
            NSLog(@"Player created and saved.");
        }
        [_nameField removeFromSuperview];
        [self loadMainMenu];
    }
    
}


-(void)loadMainMenu {
    
    
    SKAction *block = [SKAction runBlock:^{
        MainMenu *scene =
        [[MainMenu alloc]initWithSize:self.size];
        //scene.player = self.player;
        //scene.player.name = @"Zsolt";
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
    }];
    [self runAction:
     [SKAction sequence:@[
                          [SKAction waitForDuration:1],
                          block
                          ]
      ]];
    
}


-(void)didMoveToView:(SKView *)view {
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(self.size.width, self.size.height, 200, 40)];
    
    textField.center = CGPointMake(self.size.width/2, 180);
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont systemFontOfSize:17.0];
    textField.placeholder = @"Enter your name here";
    textField.backgroundColor = [UIColor whiteColor];
    //textField.autocorrectionType = UITextAutocorrectionTypeYes;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    //textField.delegate = self;
    _nameField = textField;
    [self.view addSubview:_nameField];
    
}



@end
