//
//  QuestController.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 17/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Quest.h"

@interface QuestController : NSObject

@property NSDictionary *quests;

-(Quest *)getEasyQuest;
-(Quest *)getNormalQuest;
-(Quest *)getHardQuest;
-(Quest *)getChallengeQuest;

@end
