//
//  QuestStats.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 24/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LtSScene.h"
#import "Button.h"
//#import "Quest.h"

@interface QuestStats : LtSScene <buttonDelegate>

-(instancetype)initWithSize:(CGSize)size andDetails:(NSDictionary *)details;

@end
