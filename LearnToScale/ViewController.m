//
//  ViewController.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 22/01/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "ViewController.h"
#import "SplashScreen.h"

@implementation ViewController{
    
    //Account *_user;

}

- (void)viewDidLoad {

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    //skView.showsDrawCount = YES;
    skView.showsFields = YES;
    
    //splashscreen scene
    SplashScreen *scene = [SplashScreen sceneWithSize:skView.bounds.size];
    
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
    
}

- (BOOL)shouldAutorotate
{
    return NO;
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}





@end
