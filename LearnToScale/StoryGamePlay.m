//
//  StoryGamePlay.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 19/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "StoryGamePlay.h"
#import "QuestStats.h"


const float XP_MULTIPLIER = 1.25;



@implementation StoryGamePlay{
    SKLabelNode *_hitsLabel, *_diffLabel, *_scoreLabel, *_comboLabel, *_speedLabel, *_pitchLabel, *_objectivesLabel;
    
    Quest *_currentQuest;
    int _playerSpeed;
    
    
    //NSDictionary *_req1Details;
    //NSDictionary *_req2Details;
    
    int _req1ComboTarget;
    int _req1Multiplier;
    int _req1PartsDone;
    int _req1QuestsComboTracker;
    
    int _req2ComboTarget;
    int _req2Multiplier;
    int _req2PartsDone;
    int _req2QuestComboTracker;
    
    BOOL _questObjective1Done;
    BOOL _questObjective2Done;
    BOOL _questObjectivesDone;
    
}

-(id)initWithSize:(CGSize)size andQuest:(Quest *)quest andSpeed:(float)speed {
    
    if (self = [super initWithSize:size andLevel:quest.level andSpeed: speed]) {
        
        _currentQuest = quest;
        _playerSpeed = speed;
        
        //!!!!!!!!!!!!!!!!!!!!this needs revisiting!!!!!!!!!!!!!!!!!!!!!!!!!!!  quest requirements should be independent
        
        if ([_currentQuest.requirements count] > 1) {
            NSDictionary *requirementDetails1 = [_currentQuest.requirements objectAtIndex:0];
            //NSLog(@"%@", requirementDetails1);
            _req1ComboTarget = [[requirementDetails1 objectForKey:@"combo"] intValue];
            _req1Multiplier = [[requirementDetails1 objectForKey:@"multiplier"] intValue];
            _req1PartsDone = 0;
            _req1QuestsComboTracker = -1;
            
            NSDictionary *requirementDetails2 = [_currentQuest.requirements objectAtIndex:1];
            _req2ComboTarget = [[requirementDetails2 objectForKey:@"combo"] intValue];
            _req2Multiplier = [[requirementDetails2 objectForKey:@"multiplier"] intValue];
            _req2PartsDone = 0;
            _req2QuestComboTracker = -1;
            
        }else {
            NSDictionary *requirementDetails1 = [_currentQuest.requirements objectAtIndex:0];
            _req1ComboTarget = [[requirementDetails1 objectForKey:@"combo"] intValue];
            _req1Multiplier = [[requirementDetails1 objectForKey:@"multiplier"] intValue];
            _req1PartsDone = 0;
            _req1QuestsComboTracker = -1;
            
            _req2ComboTarget = -1;
            _req2Multiplier = -1;
            _req2PartsDone = -1;
            _req2QuestComboTracker = -1;
        }
        
        
        
        _questObjective1Done = NO;
        _questObjective2Done = NO;
        _questObjectivesDone = NO;
        
       
        [self createMenuBarLayout];
        
    }
    return self;
}

-(void)doScore {
    
    self.hits++;
    self.score += self.player.level * XP_MULTIPLIER;
    self.combo++;
    [self checkQuestObjectives];
    
}

//this should be called only when a score was made
-(void)checkQuestObjectives {
    
    if (_questObjectivesDone) {
        // return if we are done alredy
        return;
    }
    
    if ([_currentQuest.requirements count] > 1) {
        
        
        //run with more requirements (2 for now)
        if (self.combo >= _req1ComboTarget) {
            
            if (_req1QuestsComboTracker == -1) {
                _req1QuestsComboTracker = 0;
                _req1PartsDone++;
            }else{
                
                _req1QuestsComboTracker++;
                
                if (_req1QuestsComboTracker >= _req1ComboTarget) {
                    _req1PartsDone++;
                    _req1QuestsComboTracker = 0;
                }
                
            }
            
        }
        //end part
        if (_req1PartsDone >= _req1Multiplier) {
            _questObjective1Done = YES;
            _objectivesLabel.text = @"1/2 Done.";
        }
        
        if (_questObjective1Done) {
           
            if (self.combo >= _req2ComboTarget) {
                if (_req2QuestComboTracker == -1) {
                    _req2QuestComboTracker = 0;
                    _req1PartsDone++;
                }else{
                    _req2QuestComboTracker++;
                    
                    if (_req2QuestComboTracker >= _req2ComboTarget) {
                        _req2PartsDone++;
                        _req2QuestComboTracker = 0;
                    }
                    
                }
            }
         
            if (_req2PartsDone >= _req2Multiplier) {
                _questObjective2Done = YES;
                _objectivesLabel.text = @"2/2 Done.";
            }
            
        }
        
        if (_questObjective1Done && _questObjective2Done) {
            _questObjectivesDone = YES;
            return;
        }
        
        
    }else {
       //with 1 requirement
        
        //1st time run?
        if (self.combo >= _req1ComboTarget) {
            
            if (_req1QuestsComboTracker == -1) {
                _req1QuestsComboTracker = 0;
                _req1PartsDone++;
            }else{
                
                _req1QuestsComboTracker++;
                
                if (_req1QuestsComboTracker >= _req1ComboTarget) {
                    _req1PartsDone++;
                    _req1QuestsComboTracker = -1;
                }
                
            }
            
        }
        //end part
        if (_req1PartsDone >= _req1Multiplier) {
            _questObjective1Done = YES;
            _questObjectivesDone = YES;
            _objectivesLabel.text = @"1/1 Done.";
            return;
        }
        
    }
    
}

-(void)addComboBonusToScore {
    
    //add animations when these happens maybe?
    
    //muiltipliers needs testing here to create a "balanced" xp system
    if (self.combo > 2) {
        if (self.combo == 5) {
            self.score += self.combo + self.player.level * XP_MULTIPLIER;
        }else if (self.combo == 10) {
            self.score += 1.15 * ((self.combo +self.player.level) * XP_MULTIPLIER);
        }else if (self.combo == 15) {
            self.score += 1.3 * ((self.combo + self.player.level) * XP_MULTIPLIER);
        }else if (self.combo == 20) {
            self.score += 1.5 * ((self.combo + self.player.level) * XP_MULTIPLIER);
        }else if (self.combo == 25) {
            self.score +=2 * ((self.combo + self.player.level) * XP_MULTIPLIER);
        }else if (self.combo == 30) {
            self.score += 2.5 * ((self.combo + self.player.level) * XP_MULTIPLIER);
        }
    }
}



-(void)updateTexts {
    
    _hitsLabel.text = [NSString stringWithFormat:@"Hits: %i", self.hits];
    _scoreLabel.text = [NSString stringWithFormat:@"Xp: %i", self.score];
    if (self.gettingInput) {
        _pitchLabel.text = self.soundEngine.currentPitch;
    }
    _comboLabel.text = [NSString stringWithFormat:@"Combo: %i", self.combo];
    
}

-(void)levelEnded {
    
    NSMutableDictionary *details = [[NSMutableDictionary alloc]init];
    
    //quest type
    [details setValue:[NSNumber numberWithInt:_currentQuest.type] forKey:@"questtype"];
    
    //quest done
    [details setValue:[NSNumber numberWithBool:_questObjectivesDone] forKey:@"questdone"];
    
    //xp earned
    [details setValue:[NSNumber numberWithInt:self.score] forKey:@"earnedxp"];
    [details setValue:[NSNumber numberWithInt:_currentQuest.xp] forKey:@"xpforquest"];
    
    //notes hit / all notes
    [details setValue:[NSNumber numberWithInt:self.hits] forKey:@"hits"];
    [details setValue:[NSNumber numberWithInt:[_currentQuest.level.notes count]] forKey:@"allnotes"];
    
    //highest combo
    [details setValue:[NSNumber numberWithInt:self.highestCombo] forKey:@"highestcombo"];
    
    //reward
    NSLog(@"Reward for this: %d", _currentQuest.reward);
    [details setValue:[NSNumber numberWithInt:_currentQuest.reward] forKey:@"reward"];
    
    //speed
    [details setValue:[NSNumber numberWithInt:_currentQuest.level.speed] forKey:@"speed"];
    
    
    
    SKTransition *transition = [SKTransition crossFadeWithDuration:1];
    QuestStats *stats = [[QuestStats alloc]initWithSize:self.size andDetails:details];
    [self.view presentScene:stats  transition:transition];
    
}

-(void)resetCombos {
    
    self.combo = 0;
    _req1QuestsComboTracker = -1;
    _req2QuestComboTracker = -1;
}


-(void)createMenuBarLayout {
    
    SKSpriteNode *menu = [SKSpriteNode spriteNodeWithImageNamed:@"menubar.png"];
    menu.anchorPoint = CGPointMake(1, 1);
    menu.yScale = 0.7;
    menu.position = CGPointMake(self.frame.size.width, self.frame.size.height);
    menu.name = @"menu";
    [self.menuLayer addChild:menu];
    
    
    //Left side
    
    _speedLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *speedStr = [NSString stringWithFormat:@"Speed: %1.0f", self.speed];
    _speedLabel.text = speedStr;
    _speedLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    _speedLabel.fontSize = 14;
    _speedLabel.position = CGPointMake(10, self.size.height -15);
    [self.menuLayer addChild:_speedLabel];
    
    
    _hitsLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *hitsStr = [NSString stringWithFormat:@"Hits: %i", self.hits];
    _hitsLabel.text = hitsStr;
    _hitsLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    _hitsLabel.fontSize = 14;
    _hitsLabel.position = CGPointMake(10, self.size.height -35);
    [self.menuLayer addChild:_hitsLabel];
    
    _comboLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *comboStr = [NSString stringWithFormat:@"Combo: %i", self.combo];
    _comboLabel.text = comboStr;
    _comboLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    _comboLabel.fontSize = 14;
    _comboLabel.position = CGPointMake(10, self.size.height -55);
    [self.menuLayer addChild:_comboLabel];
    
    
    //Middle for score
    
    _scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *scoreText = [NSString stringWithFormat:@"Xp: %i", self.score];
    _scoreLabel.fontSize = 20;
    _scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    //_scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _scoreLabel.text = scoreText;
    _scoreLabel.position = CGPointMake(self.size.width/2, self.size.height - 35);
    [self.menuLayer addChild:_scoreLabel];
    
    _pitchLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    _pitchLabel.fontSize = 15;
    _pitchLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    //_pitchLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _pitchLabel.text = @"Pitch:";
    _pitchLabel.position = CGPointMake(self.size.width/2, self.size.height - 55);
    [self.menuLayer addChild:_pitchLabel];
    
    //right side
    
    
    SKLabelNode *level = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *levelStr = [NSString stringWithFormat:@"Level: %@", _currentQuest.level.levelName];
    level.text = levelStr;
    level.fontSize = 14;
    //level.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    level.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    level.position = CGPointMake(self.size.width -10, self.size.height -15);
    [self.menuLayer addChild:level];
    /*
    _diffLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *diffStr = [NSString stringWithFormat:@"Diff: %i", self.currentLevel.difficulty];
    _diffLabel.text = diffStr;
    _diffLabel.fontSize = 14;
    _diffLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    _diffLabel.position = CGPointMake(self.size.width -10, self.size.height -35);
    [self.menuLayer addChild:_diffLabel];
    */
    SKLabelNode *allnotes = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    NSString *allnotesStr = [NSString stringWithFormat:@"Notes: %lu", (unsigned long)[self.currentLevel.notes count]];
    allnotes.text = allnotesStr;
    allnotes.fontSize = 14;
    //allnotes.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    allnotes.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    allnotes.position = CGPointMake(self.size.width -10, self.size.height -35);
    [self.menuLayer addChild:allnotes];
    
    _objectivesLabel = [SKLabelNode labelNodeWithFontNamed:@"Superclarendon"];
    _objectivesLabel.text = [NSString stringWithFormat:@"0/%d done.", (int)[_currentQuest.requirements count]];
    _objectivesLabel.fontSize = 14;
    //_objectivesLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _objectivesLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    _objectivesLabel.position = CGPointMake(self.size.width -10, self.size.height -55);
    [self.menuLayer addChild:_objectivesLabel];
    
    
    
}


@end
