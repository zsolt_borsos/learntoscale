//
//  Achievement.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 01/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Achievement : NSObject

-(Achievement *)createAchievementWithName:(NSString *)name;

@property NSString *name;
@property NSString *desc;
@property NSNumber *reward;


@end
