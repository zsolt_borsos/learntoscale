//
//  Note.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 01/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Note.h"

@implementation Note

-(id)initWithString:(int)stringNumber andNumberOn:(int)fret andPosition:(int)posY {
    
    self = [super init];
    if (self) {
        
        _stringNumber = stringNumber;
        _fret = fret;
        _posY = posY;
        _triggeredComboReset = NO;
        _midiNumber = [self getMidiNumberForFretNumber:_fret onString:_stringNumber];
     
    }
    return self;
}

//do not use this
-(SKSpriteNode *)addNoteOnString:(int)stringNumber withTheNumberOn:(int)fret atYPosition:(int)posY {
    CGFloat notePos;
    SKColor *stringColour;
    switch (stringNumber) {
            //each 52 points is a lane on the grid, starting from 30
        case 6:
            notePos = 30;
            stringColour = [SKColor redColor];
            break;
        case 5:
            stringColour = [UIColor colorWithRed:1 green:0.8 blue:0.8 alpha:1];
            notePos = 82;
            break;
        case 4:
            stringColour = [SKColor blueColor];
            notePos = 134;
            break;
        case 3:
            stringColour = [SKColor orangeColor];
            notePos = 186;
            break;
        case 2:
            stringColour = [SKColor greenColor];
            notePos = 238;
            break;
        case 1:
            stringColour = [SKColor purpleColor];
            notePos = 290;
            break;
        default:
            NSLog(@"Something went wrong? ... ");
            break;
    }
    SKSpriteNode *note = [SKSpriteNode spriteNodeWithImageNamed:@"note2.png"];
    //note.anchorPoint = CGPointZero;
    note.xScale  = 0.5;
    note.yScale = 0.5;
    note.position = CGPointMake(notePos, posY);
    note.name = @"note";
    note.userInteractionEnabled = YES;
    
    //create a label for this note
    NSString *fretText = [NSString stringWithFormat:@"%i", fret];
    SKLabelNode *label = [SKLabelNode labelNodeWithText:fretText];
    label.fontName = @"DamascusBold";
    
    //need to calculate middle!
    //label.position = CGPointMake(note.size.width/2, note.size.height/2);
    
    //manual for now
    //label.position = CGPointMake(0, -4);
    label.fontSize = 40;
    label.name = @"notel";
    label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    label.fontColor = stringColour;
    label.userInteractionEnabled = YES;
    [note addChild:label];
    
    //add bg to the game layer
    //[_bgLayer addChild:note];
    
    //return the spritenode
    return note;
    
}



-(Note *)createNoteWithOffset:(int)offset{
    
    CGFloat notePos;
    SKColor *stringColour;
    switch (_stringNumber) {
            //each 52 points is a lane on the grid, starting from 30
        case 6:
            notePos = 30;
            stringColour = [SKColor redColor];
            break;
        case 5:
            stringColour = [UIColor colorWithRed:0.5 green:0.5 blue:0 alpha:1];
            notePos = 82;
            break;
        case 4:
            stringColour = [SKColor blueColor];
            notePos = 134;
            break;
        case 3:
            stringColour = [SKColor orangeColor];
            notePos = 186;
            break;
        case 2:
            stringColour = [SKColor greenColor];
            notePos = 238;
            break;
        case 1:
            stringColour = [SKColor purpleColor];
            notePos = 290;
            break;
        default:
            NSLog(@"Something went wrong? ... ");
            break;
    }
    Note *note = [Note spriteNodeWithImageNamed:@"note2.png"];
    
    
    
    //          THIS MIGHT NEED CHANGING!
    
    //note.anchorPoint = CGPointZero;
    
    //          !!!!!!!!!!!!!!!!!!!!!!!!!
    
    
    note.xScale  = 0.5;
    note.yScale = 0.5;
    note.position = CGPointMake(notePos, offset);
    note.name = @"note";
    
    note.stringNumber = _stringNumber;
    note.fret = _fret;
    note.midiNumber = _midiNumber;
    
    //create a label for this note
    NSString *fretText = [NSString stringWithFormat:@"%i", _fret];
    SKLabelNode *label = [SKLabelNode labelNodeWithText:fretText];
    label.fontName = @"DamascusBold";
    
    //need to calculate middle!
    //label.position = CGPointMake(note.size.width/2, note.size.height/2);
    
    //manual for now
    //label.position = CGPointMake(0, -4);
    label.fontSize = 40;
    label.name = @"notel";
    label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    label.fontColor = stringColour;
    [note addChild:label];
    return note;
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    /*
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        if ([self containsPoint:location] == YES) {
            [self.delegate noteShouldBeRemoved:self];
            [self.delegate successfulHitOnNote];
        }
    }
    */
    [self.delegate noteShouldBeRemoved:self];
    [self.delegate successfulHitOnNote];
}


//for standard tuning only
-(float)getMidiNumberForFretNumber:(int)fret onString:(int)string {
    int midiNumber;
    
    switch (string) {
        case 1:
            midiNumber = 64 + fret;
            break;
        case 2:
            midiNumber = 59 + fret;
            break;
        case 3:
            midiNumber = 55 + fret;
            break;
        case 4:
            midiNumber = 50 + fret;
            break;
        case 5:
            midiNumber = 45 + fret;
            break;
        case 6:
            midiNumber = 40 + fret;
            break;
        default:
            NSLog(@"Wrong string number??");
            break;
    }
    
    return midiNumber;
}


@end
