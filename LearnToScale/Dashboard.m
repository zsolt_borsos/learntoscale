//
//  Dashboard.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Dashboard.h"
#import "LevelGenerator.h"
#import "MainMenu.h"
#import "SelectScale.h"
#import "SelectDifficulty.h"
#import "SelectSpeed.h"
#import "SelectComplexity.h"
#import "Tuner.h"
#import "PracticeGameplay.h"

@implementation Dashboard{
    
    SKNode *_bgLayer;
    NSDictionary *_scales;
    int _root;
    NSString *_scale;
    int _difficulty;
    int _reward;
    SKLabelNode *_pitchLabel;
    SoundEngine *_soundEngine;
    //PdDispatcher *_dispatcher;
    int _costToPlay;
    int _levelSpeed;
    NSString *_complexity;
    Button *_soundButton;
    BOOL _soundOn;
}
 
- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        
        _soundEngine = [SoundEngine sharedSound];
        if (!_soundEngine.loaded) {
            UIViewController *vc = self.view.window.rootViewController;
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Attention" message:@"Failed to load sound patch. Please restart the app." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *defAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //some stuff here?
            }];
            [alert addAction:defAction];
            [vc presentViewController:alert animated:YES completion:nil];
        }
        
        _soundOn = _soundEngine.soundOn;
        
        
        
        [self loadScales];
        
        //these needs sorting
        //_costToPlay = 1;
        _reward = 0;
        
        _root = self.player.lastRoot;
        
        _difficulty = self.player.lastDiff;
        if (_difficulty == 0) {
            _difficulty = 1;
        }
        
        _levelSpeed = self.player.lastSpeed;
        if (!_levelSpeed) {
            _levelSpeed = 1;
        }
        
        _scale = self.player.lastScale;
        if (!_scale) {
            _scale = @"Major";
        }
        
        _complexity = self.player.lastComplexity;
        if (!_complexity) {
            _complexity = @"Fixed";
        }
        
        
        _bgLayer = [[SKNode alloc]init];
        [self addChild:_bgLayer];
        [self createLayout];
        [self.player save];
        
    }
    return self;
}


-(void)update:(NSTimeInterval)currentTime {
    
    if (_soundEngine.currentPitchMidiNumber != 0) {
        _pitchLabel.text = _soundEngine.currentPitch;
    }else{
        _pitchLabel.text = @"~";
    }
    
   
}


-(void)loadScales {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"scales" ofType:@"plist"];
    _scales = [NSDictionary dictionaryWithContentsOfFile:path];
    
    //debug
    /*
    for (NSArray *scale in _scales) {
        NSLog(@"Found Scale: %@", scale.description);
    }
    */
    
    
}


-(NSString *)getRootString:(int)rootId {
    
    NSString *str;
    
    switch (rootId) {
        case 0:
            str = @"C";
            break;
        case 1:
            str = @"C#";
            break;
        case 2:
            str = @"D";
            break;
        case 3:
            str = @"D#";
            break;
        case 4:
            str = @"E";
            break;
        case 5:
            str = @"F";
            break;
        case 6:
            str = @"F#";
            break;
        case 7:
            str = @"G";
            break;
        case 8:
            str = @"G#";
            break;
        case 9:
            str = @"A";
            break;
        case 10:
            str = @"A#";
            break;
        case 11:
            str = @"B";
            break;
        default:
            str = @"Erm?";
            break;
    }
    
    return str;
}

/*
-(NSString *)getComplexityStringFromInt:(int)complexity {
    
    NSString *str;
    
    switch (complexity) {
        case 0:
            str = @"Robot";
            break;
        case 1:
            str = @"Shuffle";
            break;
        case 2:
            str = @"Random";
            break;
        default:
            NSLog(@"Not selected a complexity?");
    }
    return  str;
}
*/
 
-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"back"] == YES) {
        MainMenu *scene = [[MainMenu alloc]initWithSize:self.size];
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
    }
    
    if ([name isEqualToString:@"startLevel"] == YES) {
        
        
        NSArray *selectedScale = [_scales objectForKey:_scale];
        LevelGenerator *practiceLevel = [[LevelGenerator alloc]initWithArrayOfNotesInScale:selectedScale onRoot:_root andName:_scale andDifficulty:_difficulty andRewards:_reward];
        
        PracticeGameplay *gp = [[PracticeGameplay alloc]initWithSize:self.size andLevel:[practiceLevel generateEasyLevel] andSpeed:72];
        SKTransition *transition = [SKTransition crossFadeWithDuration:0.5];
        [self.view presentScene:gp transition:transition];
    }
    
    if ([name isEqualToString:@"diffButton"] == YES) {
        
        SelectDifficulty *scene = [[SelectDifficulty alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsOpenVerticalWithDuration:0.5];
        [self.view presentScene:scene transition:transition];
        
    }
    
    if ([name isEqualToString:@"scaleButton"] == YES) {
        
        SelectScale *scene = [[SelectScale alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsOpenVerticalWithDuration:0.5];
        [self.view presentScene:scene transition:transition];
        
    }
        
    if ([name isEqualToString:@"rootButton"] == YES) {
        //
    }
    
    if ([name isEqualToString:@"challenge"] == YES) {
        //
    }
    
    if ([name isEqualToString:@"complexity"] == YES) {
        SelectComplexity *scene = [[SelectComplexity alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition flipHorizontalWithDuration:0.5];
        [self.view presentScene:scene transition:transition];
    }
    
    if ([name isEqualToString:@"sound"] == YES) {

        [_soundEngine soundTrigger];
        if (_soundOn) {
            _soundOn = NO;
            [_soundButton changeTextTo:@"Sound On"];
        }else {
            _soundOn = YES;
            [_soundButton changeTextTo:@"Sound Off"];
        }
        
    }
    
    
    if ([name isEqualToString:@"speedButton"] == YES) {
        SelectSpeed *scene = [[SelectSpeed alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsOpenHorizontalWithDuration:0.5];
        [self.view presentScene:scene transition:transition];
    }
    
    NSLog(@"Button pressed in Dashboard: %@", name);
}


-(void)createLayout {
    
    SKSpriteNode *bg = [[SKSpriteNode alloc]initWithImageNamed:@"menubg.png"];
    [_bgLayer addChild:bg];
    
    
    //settings
    
    /*
     SKSpriteNode *settings = [SKSpriteNode spriteNodeWithImageNamed:@"settings.png"];
     [settings setScale:0.7];
     settings.position = CGPointMake(self.size.width - 30, self.size.height - 30);
     settings.name = @"settings";
     [_menuLayer addChild:settings];
     */
    
    
    
    //top display
    SKLabelNode *picks = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    NSString *pickStr = [NSString stringWithFormat:@"Picks: %i", self.player.picks];
    picks.text = pickStr;
    picks.fontSize = 15;
    picks.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    picks.position = CGPointMake(5, self.size.height-20);
    [_bgLayer addChild:picks];
    
    //top pick timer -> maybe update this real time too?
    
    /*
    SKLabelNode *nextPick = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    NSString *nextPicktr;
    int nextPickTime = [self getNextPickDueInTime];
    if (nextPickTime < 1) {
        nextPicktr = [NSString stringWithFormat:@"Next pick: <1min"];
    }else {
        nextPicktr = [NSString stringWithFormat:@"Next pick: %dmins ", nextPickTime];
    }
    nextPick.text = nextPicktr;
    nextPick.fontSize = 15;
    nextPick.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
    nextPick.position = CGPointMake(self.size.width, self.size.height-20);
    [_bgLayer addChild:nextPick];
    */
    
    //show pitch on-the-fly
    _pitchLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    //NSString *pitchLabelStr = [NSString stringWithFormat:@"Note:"];
    _pitchLabel.text = @"Note:";
    _pitchLabel.fontSize = 15;
    _pitchLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    _pitchLabel.position = CGPointMake(self.size.width/2, self.size.height-20);
    [_bgLayer addChild:_pitchLabel];
    
    
    
    
    
    //buttons
    
    //top 2 main buttons -> might need some text labels here to display costs of play!
    
    //challenge me button
   
    /*
    
    Button *challengeb = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(140, 80) withText:@"Challenge" andFontSize:24 andName:@"challenge"];
    challengeb.position = CGPointMake(self.size.width - 85, self.size.height - 80);
    //challengeb.anchorPoint = CGPointZero;
    challengeb.userInteractionEnabled = YES;
    challengeb.delegate = self;
    [_bgLayer addChild:challengeb];
    
     */
    
    //start level button
    Button *startLevelb = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(290, 70) withText:@"Start Level" andFontSize:30 andName:@"startLevel"];
    startLevelb.position = CGPointMake( self.size.width/2, self.size.height - 70);
    startLevelb.userInteractionEnabled = YES;
    startLevelb.delegate = self;
    [_bgLayer addChild:startLevelb];
    
    
    
    //level settings buttons
    
    
    //select scale button -> full length as this is the main thing!!
    NSString *scaleStr = [NSString stringWithFormat:@"Scale: %@", _scale];
    Button *scaleButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(290, 50) withText:scaleStr andFontSize:20 andName:@"scaleButton"];
    scaleButton.position = CGPointMake( self.size.width/2, ((self.size.height / 4)* 3)- 50);
    scaleButton.userInteractionEnabled = YES;
    scaleButton.delegate = self;
    [_bgLayer addChild:scaleButton];
    
    
    //select complexity
    NSString *compStr = [NSString stringWithFormat:@"Complexity: %@", _complexity];
    Button *complexityButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(290, 50) withText:compStr andFontSize:20 andName:@"complexity"];
    complexityButton.position = CGPointMake( self.size.width/2, (self.size.height / 4) * 3 - 110);
    complexityButton.userInteractionEnabled = YES;
    complexityButton.delegate = self;
    [_bgLayer addChild:complexityButton];
    
    
    /*
    //select root button -> left side/half size
    NSString *rootStr = [NSString stringWithFormat:@"Root: %@", [self getRootString:_root]];
    Button *rootButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(140, 50) withText:rootStr andFontSize:20 andName:@"rootButton"];
    rootButton.position = CGPointMake( 85, (self.size.height / 4) * 3 - 170);
    rootButton.userInteractionEnabled = YES;
    rootButton.delegate = self;
    [_bgLayer addChild:rootButton];
    */
    
    
    //select difficulty -> right side/ half size
    NSString *diffStr = [NSString stringWithFormat:@"Difficulty: %d", _difficulty];
    Button *diffButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(290, 50) withText:diffStr andFontSize:20 andName:@"diffButton"];
    diffButton.position = CGPointMake( self.size.width/2, (self.size.height / 4) * 3 - 170);
    diffButton.userInteractionEnabled = YES;
    diffButton.delegate = self;
    [_bgLayer addChild:diffButton];
    
    
    //select speed
    
    NSString *speedStr = [NSString stringWithFormat:@"Speed: %i", _levelSpeed];
    Button *speedButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(290, 50) withText:speedStr andFontSize:20 andName:@"speedButton"];
    speedButton.position = CGPointMake( self.size.width/2 , (self.size.height / 4) * 3 - 230);
    speedButton.userInteractionEnabled = YES;
    speedButton.delegate = self;
    [_bgLayer addChild:speedButton];
    
    
    //need to add length button/selection!
    
    /*
    //tuner
    Button *tunerButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(290, 50) withText:@"Tuner" andFontSize:20 andName:@"tuner"];
    tunerButton.position = CGPointMake(self.size.width/2, 100);
    tunerButton.userInteractionEnabled = YES;
    tunerButton.delegate = self;
    [_bgLayer addChild:tunerButton];
    */
    
    //sound on/off button
    _soundButton = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(140, 50) withText:@"Sound On" andFontSize:20 andName:@"sound"];
    _soundButton.position = CGPointMake(self.size.width - 85, 40);
    _soundButton.userInteractionEnabled = YES;
    _soundButton.delegate = self;
    [_bgLayer addChild:_soundButton];
    
    if (_soundOn) {
        [_soundButton changeTextTo:@"Sound Off"];
    }else {
        [_soundButton changeTextTo:@"Sound On"];
    }
    
    
    //back button
    Button *backb = [[Button alloc]initWithColor:[SKColor orangeColor] size:CGSizeMake(140, 50) withText:@" <--" andFontSize:50 andName:@"back"];
    backb.position = CGPointMake(85, 40);
    backb.userInteractionEnabled = YES;
    backb.delegate = self;
    [_bgLayer addChild:backb];
    
}






@end
