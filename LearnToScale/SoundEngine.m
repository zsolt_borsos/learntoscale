//
//  SoundEngine.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 10/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "SoundEngine.h"
#import <AVFoundation/AVFoundation.h>

@implementation SoundEngine{
    
    int _pageFoldingCounter;
    //BOOL _isBgPlaying;
}

void fiddle_tilde_setup();

@synthesize currentPitch;
@synthesize currentPitchMidiNumber;
@synthesize currentPrecisePitch;
@synthesize loaded;


+(id)sharedSound {
    static SoundEngine *sharedSound = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedSound = [[self alloc] init];
    });
    return sharedSound;
    
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _bgPlaying = NO;
        _soundOn = NO;
        _pageFoldingCounter = 1;
        
        //create dispatcher
        dispatcher = [[PdDispatcher alloc] init];

        //moving these in the scenes
        //trying to get these from the patch
        [dispatcher addListener:self forSource:@"pitch"];
        [dispatcher addListener:self forSource:@"midinum"];
        [dispatcher addListener:self forSource:@"tunerPitch"];

        
        
        //add external
        fiddle_tilde_setup();
        
        //set PdBase delegate
        [PdBase setDelegate:dispatcher];
        
        //load patch
        patch = [PdBase openFile:@"tuner3.pd" path:[[NSBundle mainBundle] resourcePath]];
        if (!patch) {
            NSLog(@"Failed to open patch!");
            loaded = NO;
        }else {
            loaded = YES;
            //[self triggerBgMusic];
        }

    }
    return self;
}



-(void)receiveMessage:(NSString *)message withArguments:(NSArray *)arguments fromSource:(NSString *)source {
    //NSLog(@"Message from %@: %@", source, message);
    for (NSString *msg in arguments) {
        currentPitch = msg;
        //NSLog(@"note: %@", msg);
    }
}

-(void)receiveFloat:(float)received fromSource:(NSString *)source {
    
    //NSLog(@"Recieved a float: %f from %@", received, source);
    if ([source isEqualToString:@"midinum"] == YES) {
        currentPitchMidiNumber = received;
    }else if ([source isEqualToString:@"tunerPitch"] == YES) {
        currentPrecisePitch = received;
    }
}


-(void)setMyDelegateTo:(PdDispatcher *)delegate {
    [PdBase setDelegate:nil];
    [PdBase setDelegate:delegate];
    
}


-(void)dealloc {
    [PdBase closeFile:patch];
    [PdBase setDelegate:nil];
}

-(void)soundTrigger{
    
    /*
    if (_soundOn) {
        _soundOn = NO;
    }else {
        _soundOn = YES;
    }
    */
    
    _soundOn = !_soundOn;
    [PdBase sendBangToReceiver:@"soundTrigger"];
    
}

-(void)playBuySound {
    [PdBase sendBangToReceiver:@"playBuy"];
}

-(void)playPageFoldingSound {

    switch (_pageFoldingCounter) {
        case 1:
            [PdBase sendBangToReceiver:@"playPage1"];
            
            _pageFoldingCounter = 2;
            break;
        case 2:
            [PdBase sendBangToReceiver:@"playPage2"];
            
            _pageFoldingCounter = 1;
            break;
        default:
            [PdBase sendBangToReceiver:@"playPage1"];
           
            break;
    }
    
}

-(void)playLevelUpSound {
    [PdBase sendBangToReceiver:@"playLevelUp1"];
}


-(void)triggerBgMusic {
    if (_bgPlaying) {
        [PdBase sendBangToReceiver:@"stopBgMusic"];
        _bgPlaying = NO;
    }else{
        [PdBase sendBangToReceiver:@"playBgMusic"];
        _bgPlaying = YES;
    }
}

-(void)stopBgMusic {
    [PdBase sendBangToReceiver:@"stopBgMusic"];
    _bgPlaying = NO;
    
}

-(void)playEasyDrums{
    [PdBase sendBangToReceiver:@"playEasyDrums"];
}

-(void)stopEasyDrums {
    [PdBase sendBangToReceiver:@"stopEasyDrums"];
}

@end
