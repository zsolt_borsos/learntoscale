//
//  Gameplay.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//


#import "Gameplay.h"
#import "SKTUtils/SKTUtils.h"
#import "LevelStatsScreen.h"


//static const float BG_POINTS_PER_SEC = 60.0;

@implementation Gameplay{
    
    float _posForNote;
    BOOL _runOnce;
    BOOL _levelLoaded;
    float _dtSum;
    
}

-(id)initWithSize:(CGSize)size andLevel:(Level *)level andSpeed:(float)speed {
    
    if (self = [super initWithSize:size]) {
        
        _currentLevel = level;
        _levelLoaded = NO;
        _dtSum = 0;
        self.speed = speed;
        
        _soundEngine = [SoundEngine sharedSound];
        [_soundEngine stopBgMusic];
        
        
        _bgLayer = [SKNode node];
        [self addChild:_bgLayer];
        
        _menuLayer = [SKNode node];
        [self addChild:_menuLayer];
        
        [self loadBackground];
        
        _playerPosition = self.size.height / 6;
        
        [self addPlayerLine];
        
        //set defaults
        _highestCombo = 0;
        _score = 0;
        _combo = 0;
        _hits = 0;
        _currentPlayedPitch = 0;
        
        _mapFinished = NO;
        _loadOnce = NO;
        
        [self runCurrentLevel];
        
        [self createMenuBarLayout];
        
        self.levelDelegate = self;
        
        _runOnce = NO;
        
    }
    return self;
}


// these comes from the subclasses

-(void)doScore {

}

-(void)updateTexts {
    
}

-(void)createMenuBarLayout {

}



//these are the same on all gameplay modes (for now)

-(void)loadLevel:(Level *)level {
    
    _currentLevel = level;
    
}

-(void)runCurrentLevel {
    
    //float movePerSec = (60 / self.speed) * 100;
    
    float startY = _currentLevel.speed * 8;
    
    _posForNote = -1;
    
    
    for (Note *n in _currentLevel.notes) {
        n.delegate = self;
        if (_posForNote == -1) {
            _posForNote = startY;
        }else{
            _posForNote += [self getNoteDistance:n.posY];
        }
        
       
        
        NSLog(@"String: %d fret: %d midinum: %f posOnY: %f", n.stringNumber, n.fret, n.midiNumber, _posForNote);
        
        [_bgLayer addChild:[n createNoteWithOffset: _posForNote]];
        
        
        
        
        
        /* doesn't work :( need to syncronise some other way!
         
        float pointToMove = [self getNoteDistance:n.posY];
        CGPoint bgVelocity = CGPointMake(0, -pointToMove);
        CGPoint amtToMove = CGPointMultiplyScalar(bgVelocity, _dt);
        CGPoint p = CGPointAdd(n.position, amtToMove);
        NSLog(@"p.y: %f", p.y);
        
        
        _posForNote += (p.y + [self getNoteDistance:n.posY]);
        */
        
    }
    
    NSLog(@"Added %i notes to level.", (int)[_currentLevel.notes count]);
}


//quarterBeatNum: 1-4. Using it to determine distance between notes. Using common tempo 4/4 for now.
-(float)getNoteDistance:(int)quarterBeatNum {
    
    //hacky fix for speed for now
    float movePerSec = self.currentLevel.speed;
    
    if (_currentLevel.speed == 60) {
        movePerSec = (self.currentLevel.speed * 2);
    }
    
    //((60 / self.speed) * 100) * 2;
    
    switch (quarterBeatNum) {
        case 1:
            return movePerSec / 4;
            break;
        case 2:
            return movePerSec / 2;
            break;
        case 3:
            return ((movePerSec / 4) * 3);
            break;
        case 4:
            return movePerSec;
        default:
            return movePerSec;
            break;
    }
    
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
 
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        SKNode *target = [self nodeAtPoint:location];
     
        if ([target.name  isEqualToString: @"note"]) {
            [target removeFromParent];
            [_currentLevel.notes removeObject:target];
            [self doScore];
            [self addComboBonusToScore];
            [self updateTexts];
            
        }else if ([target.name  isEqualToString: @"notel"]) {
            [target.parent removeFromParent];
            [_currentLevel.notes removeObject:target.parent];
            [self doScore];
            [self addComboBonusToScore];
            [self updateTexts];
            
        }
        
    }
}


-(void)checkForEndOfLevel {
    
    //Note *last = _currentLevel.notes.lastObject;
    //NSLog(@"positionY: %f bgPosY: %f", last.posY,  _bgLayer.position.y);
    if (-(_posForNote + self.size.height/2) > _bgLayer.position.y) {
        _mapFinished = YES;
        [_soundEngine stopEasyDrums];
    }
    
    
}



-(void)update:(CFTimeInterval)currentTime {
   
    //get delta for consistent frame updates
    if (_lastUpdateTime) {
        _dt = currentTime - _lastUpdateTime;
        //_dtSum += _dt;
    }else {
        _dt = 0;
    }
    _lastUpdateTime = currentTime;
    
    if (_mapFinished) {
        if (_loadOnce == NO) {
            [self doWinStuff];
        }
    }else{
        
        //move the background
        
        [self moveBgLayerwithSpeed: self.speed];
        
        [self checkForScorableNotes];
        
        if (_soundEngine.currentPitchMidiNumber != 0) {
            _gettingInput = YES;
        }else{
            _gettingInput = NO;
        }
        
        [self updateTexts];
        
        [self checkForEndOfLevel];
        
        if (!_runOnce) {
            
            [self turnDrumsOn];
            
        }
        //*/
        //[self updateNotesPosition];
        
    }
    
}



-(void)turnDrumsOn {
    [_bgLayer enumerateChildNodesWithName:@"note" usingBlock:^(SKNode *node, BOOL *stop) {
        
        CGPoint pos = [_bgLayer convertPoint:node.position toNode:self];
        
        if (pos.y <= _playerPosition + (_currentLevel.speed * 8)) {
            
            [_soundEngine playEasyDrums];
            NSLog(@"starting sound");
             _runOnce = YES;
        }
    }];    
}

//check for notes on the yellow line

-(void)checkForScorableNotes {
    
    [_bgLayer enumerateChildNodesWithName:@"note" usingBlock:^(SKNode *node, BOOL *stop) {
        
        CGPoint pos = [_bgLayer convertPoint:node.position toNode:self];
        Note *n = (Note *)node;
        
        if (pos.y <= _playerPosition + 5) {
            if (pos.y > _playerPosition - 20) {
                if (_soundEngine.currentPitchMidiNumber == n.midiNumber) {
                    [n removeFromParent];
                    if (_highestCombo < _combo) {
                        _highestCombo = _combo;
                    }
                    [self doScore];
                    [self addComboBonusToScore];
                    [self updateTexts];
                }
            }
        }
        
        if (pos.y < _playerPosition - 21){
            if (!n.triggeredComboReset) {
                [self resetCombos];
                n.triggeredComboReset = YES;
            }
        }
    }];
}

-(void)didMoveToView:(SKView *)view {
    
    //[_soundEngine playEasyDrums];
    
}

-(void)addComboBonusToScore {
    
    
}

-(void)resetCombos {
    
    _combo = 0;
}

-(void)doWinStuff {
    
    //adding last combo to score before finishing.
    //[self addComboBonusToScore];
    
    NSLog(@"We won!");
    _loadOnce = YES;
    [self.levelDelegate levelEnded];
    
}

-(void)levelEnded {
    
    
    SKTransition *transition = [SKTransition crossFadeWithDuration:0.5];
    LevelStatsScreen *stats = [[LevelStatsScreen alloc]initWithSize:self.size];
    [self.view presentScene:stats  transition:transition];
    
}

//source: ZombieConga
-(void)loadBackground {
    
    
    for (int i = 0; i < 2; i++) {
        SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"GameplayBg.png"];
        bg.anchorPoint = CGPointZero;
        bg.xScale  = 0.5;
        bg.yScale = 1;
        bg.position = CGPointMake(0, i * bg.size.height);
        bg.name = @"bg";
        //add bg to the game layer
        [_bgLayer addChild:bg];
    }
}

-(void)addPlayerLine {
    
    SKSpriteNode *line = [SKSpriteNode spriteNodeWithColor:[SKColor yellowColor] size:CGSizeMake(self.size.width, 10)];
    line.position = CGPointMake(self.size.width/2, _playerPosition);
    line.name = @"playerLine";
    [self addChild:line];
}

// source: Zombieconga
-(void)moveBgLayerwithSpeed:(float)speed
{
    float pointToMove = speed;
    //(float)((BG_POINTS_PER_SEC / speed) * 100)*2;
    CGPoint bgVelocity = CGPointMake(0, -pointToMove);
    CGPoint amtToMove = CGPointMultiplyScalar(bgVelocity, _dt);

    //NSLog(@"dt: %f", _dt);
    
    _bgLayer.position = CGPointAdd(_bgLayer.position, amtToMove);
    
    //_bgLayer.position = CGPointMake(_bgLayer.position.x, _bgLayer.position.y - pointToMove);
    
    [_bgLayer enumerateChildNodesWithName:@"bg" usingBlock:^(SKNode *node, BOOL *stop) {
        
        SKSpriteNode *bg = (SKSpriteNode *)node;
        CGPoint bgScreenPos = [_bgLayer convertPoint:bg.position toNode:self];
        if (bgScreenPos.y <= -bg.size.height) {
            bg.position = CGPointMake(bg.position.x, bg.position.y + bg.size.height * 2);
            
        }
        
    }];
}





@end
