//
//  PresentQuest.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 21/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LtSScene.h"
#import "Quest.h"
#import "Button.h"

@interface PresentQuest : LtSScene <buttonDelegate>


-(id)initWithSize:(CGSize)size andQuest:(Quest *)quest;

@end
