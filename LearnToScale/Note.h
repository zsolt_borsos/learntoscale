//
//  Note.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 01/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@protocol hitNoteDelegate <NSObject>

@optional
-(void)noteShouldBeRemoved:(id)note;
-(void)successfulHitOnNote;

@end

@interface Note : SKSpriteNode

@property (nonatomic,weak)id<hitNoteDelegate>delegate;



//-(SKSpriteNode *)addNoteOnString:(int)stringNumber withTheNumberOn:(int)fret atYPosition:(int)posY;


//use it to create the note. This note then to be stored in the level's notes array.
-(id)initWithString:(int)stringNumber andNumberOn:(int)fret andPosition:(int)posY;

@property int stringNumber;
@property int fret;
//this is distance now
@property int posY;
@property float bendTo;
@property float lengthToPlay;
@property float slideTo;
@property float sliedFrom;
@property float midiNumber;
@property BOOL triggeredComboReset;

//use it to add the note to the gameplay area
-(Note *)createNoteWithOffset:(int)offset;


@end
