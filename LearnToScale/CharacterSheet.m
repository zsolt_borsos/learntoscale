//
//  CharacterSheet.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 20/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "CharacterSheet.h"
#import "MainMenu.h"
#import "SKTAudio.h"
#import "SoundEngine.h"
#import "QuestController.h"
#import "PresentQuest.h"


@implementation CharacterSheet{
    
    SKNode *_bgLayer;
    SKNode *_topLayer;
    SKNode *_bottomLayer;
    SKNode *_upgradesLayer;
    SKNode *_questsLayer;
    SKAction *_appear;
    SKAction *_disappear;

    int _quarterWidth;
    Button *_normalButton, *_hardButton, *_challengeButton;
    
    //from UI: speed:0, diff:1, comp:2
    int _currentSelectedUpgradeId;
    SKNode *_popupWindow;
    SKAction *_popupDismiss;
    SKAction *_popupAppear;
    SoundEngine *_soundEngine;
    
    //labels that needs refreshing
    SKLabelNode *_charPick;
    SKLabelNode *_charLevel;
    SKLabelNode *_charXpValue;
    
    NSString *_levelName;
    int _rootId;
    
    //quests
    int _levelLimit;
    int _randomPosition;
    NSString *_questName;
    int _randomRootPos;
    int _randomDiffPos;
    int _randomCompPos;
    int _randomSpeedPos;
    
    QuestController *_questController;
    
    
}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        //get the shared sound engine
        _soundEngine = [SoundEngine sharedSound];
        
        //set for -1 to flag its not in use
        _currentSelectedUpgradeId = -1;
        
        //quest controller
        _questController = [[QuestController alloc]init];
        
        _appear = [SKAction scaleXTo:1 duration:0.5];
        _disappear  = [SKAction scaleXTo:0.3 duration:0.2];
        _popupDismiss = [SKAction sequence:@[

            [SKAction moveTo:CGPointMake(self.size.width/3, self.size.height/3) duration:0.4],
            [SKAction scaleTo:0 duration:0.7],
                        [SKAction customActionWithDuration:0.5 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
                        [self updateLabels];
        }]
            ]];
        _popupAppear = [SKAction sequence:@[
                                            [SKAction scaleTo:1 duration:0.4],
                                            //[SKAction mo]
                                            
                                            ]];
        _quarterWidth = self.size.width / 4;
        [self createBackgrounds];
        [self createFixedTopLayout];
        [self createBottomUpgradeLayout];
        [self createBottomQuestsLayout];
        [_bottomLayer addChild:_questsLayer];
        
        _normalButton.hidden = !self.player.normalQuestAvailable;
        _hardButton.hidden = !self.player.hardQuestAvailable;
        _challengeButton.hidden = !self.player.challangeQuestAvailable;
        
        
        //testing stuff / cheat
        
        //self.player.easyQuestsDone = 9;
        //self.player.normalQuestsDone = 9;
        //self.player.hardQuestsDone = 2;
        //[self.player save];
        

        
    }
    return self;
}



-(int)getRandomNumberWithMax:(int)max {
    
    return (arc4random_uniform(max));
    
}

-(void)buttonPressedWithName:(NSString *)name {
    
    //provide upgradeprices for all buttons. Maybe move it while refactoring to subcategories?
    NSString *path = [[NSBundle mainBundle] pathForResource:@"upgradePrices" ofType:@"plist"];
    NSArray *upgradePrices = [NSArray arrayWithContentsOfFile:path];
    
    if ([name isEqualToString:@"easyButton"]) {
        
        
        //new way
        //get questController and create a new easy quest
        
        
        PresentQuest *scene = [[PresentQuest alloc]initWithSize:self.size andQuest:[_questController getEasyQuest]];
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
  
    }
    
    if ([name isEqualToString:@"normalButton"]) {
        
        
        //new way
       
        PresentQuest *scene = [[PresentQuest alloc]initWithSize:self.size andQuest:[_questController getNormalQuest]];
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
        
    }
    if ([name isEqualToString:@"hardButton"]) {
    
        //new way
     
        PresentQuest *scene = [[PresentQuest alloc]initWithSize:self.size andQuest:[_questController getHardQuest]];
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
    
    }
   
    
    if ([name isEqualToString:@"challengeButton"]) {
        
        
        //new way
        
        PresentQuest *scene = [[PresentQuest alloc]initWithSize:self.size andQuest:[_questController getChallengeQuest]];
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
        
    }
    
    if ([name isEqualToString:@"backButton"]) {
        MainMenu *scene = [[MainMenu alloc]initWithSize:self.size];
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
    }
    
    if ([name isEqualToString:@"questButton"]) {
        
        [_upgradesLayer runAction:_disappear completion:^{
            [_upgradesLayer removeFromParent];
            _questsLayer.xScale = 0.3;
            [_bottomLayer addChild:_questsLayer];
            [_questsLayer runAction:_appear];
        }];
        [_soundEngine playPageFoldingSound];
    }
    if ([name isEqualToString:@"upgradesButton"]) {
        
        [_questsLayer runAction:_disappear completion:^{
            [_questsLayer removeFromParent];
            //[_upgradesLayer setScale:0];
            _upgradesLayer.xScale = 0.3;
            [_bottomLayer addChild:_upgradesLayer];
            [_upgradesLayer runAction:_appear];
        }];
        [_soundEngine playPageFoldingSound];
    }
    
    if ([name isEqualToString:@"speedButton"]) {
        
        //to track what we need to upgrade!
        _currentSelectedUpgradeId = 0;
        
        NSNumber *cost = [upgradePrices objectAtIndex:self.player.speedUpgradeLevel];
        _popupWindow = nil;
        _popupWindow = [self createPopupWindowWithSize:CGSizeMake(self.size.width-35, self.size.height - 205)andPosition:CGPointMake(self.size.width/2, self.size.height/2 - 85)forUpgrade:@"Speed" andCost:[cost intValue]];
        [_popupWindow setScale:0];
        [self addChild:_popupWindow];
        [_popupWindow runAction:_popupAppear];
        
    }
    
    if ([name isEqualToString:@"diffButton"]) {
    
        //to track what we need to upgrade!
        _currentSelectedUpgradeId = 1;
        
        NSNumber *cost = [upgradePrices objectAtIndex:self.player.diffUpgradeLevel];
        _popupWindow = nil;
        _popupWindow = [self createPopupWindowWithSize:CGSizeMake(self.size.width-35, self.size.height - 205)andPosition:CGPointMake(self.size.width/2, self.size.height/2 - 85)forUpgrade:@"Difficulty" andCost:[cost intValue]];
        [_popupWindow setScale:0];
        [self addChild:_popupWindow];
        [_popupWindow runAction:_popupAppear];
    }
    
    
    if ([name isEqualToString:@"compButton"]) {
        
        _currentSelectedUpgradeId = 2;
        
        NSNumber *cost = [upgradePrices objectAtIndex:self.player.compUpgradeLevel];
        _popupWindow = nil;
        _popupWindow = [self createPopupWindowWithSize:CGSizeMake(self.size.width-35, self.size.height - 205)andPosition:CGPointMake(self.size.width/2, self.size.height/2 - 85)forUpgrade:@"Complexity" andCost:[cost intValue]];
        [_popupWindow setScale:0];
        [self addChild:_popupWindow];
        [_popupWindow runAction:_popupAppear];
        
    }
    
    
    if ([name isEqualToString:@"upgradeBack"]) {
        [_popupWindow runAction:_popupDismiss];
    }

    //check "popup window" property to see what is getting upgraded
    if ([name isEqualToString:@"upgradeBuy"]) {
        
        
        [_popupWindow runAction:_popupDismiss];
        
        //currentSelectedUpgradeId is updated on each upgrade button
        switch (_currentSelectedUpgradeId) {
            case 0:
                if ([self.player upgradeSpeed]) {
                    [_soundEngine playBuySound];
                    
                }else {
                    NSLog(@"Failed to upgrade!");
                }
                break;
            case 1:
                if ([self.player upgradeDiff]) {
                    [_soundEngine playBuySound];
                    
                }else {
                    NSLog(@"Failed to upgrade!");
                }
                break;
            case 2:
                if ([self.player upgradeComp]) {
                    [_soundEngine playBuySound];
                    
                }else {
                    NSLog(@"Failed to upgrade!");
                }
                break;
            case 3:
                if ([self.player upgradeRoot:_rootId]) {
                    [_soundEngine playBuySound];
                    
                }else {
                    NSLog(@"Failed to upgrade!");
                }
                break;
            case 4:
                if ([self.player upgradeLevel:_levelName]) {
                    [_soundEngine playBuySound];
                    
                }else {
                    NSLog(@"Failed to upgrade!");
                }
                break;
                

            default:
                NSLog(@"No upgrade for you!");
                break;
        }
        
    }

}


-(void)createBackgrounds {
    _bgLayer = [[SKNode alloc]init];
    //SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"CharacterSheetBg.png"];
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor:[SKColor whiteColor] size:self.size];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    [self addChild:_bgLayer];
    
    
    _topLayer = [[SKNode alloc]init];
    SKSpriteNode *topBg = [SKSpriteNode spriteNodeWithImageNamed:@"CharacterSheet2.png"];
    topBg.anchorPoint = CGPointMake(0, 1);
    topBg.position = CGPointMake(7, self.size.height-7);
    //[topBg setScale:0.5];
    topBg.xScale = 0.5;
    topBg.yScale = 0.6;
    [_topLayer addChild:topBg];
    [_bgLayer addChild:_topLayer];
    
    _bottomLayer = [[SKNode alloc]init];
    SKSpriteNode *bottomBg = [SKSpriteNode spriteNodeWithImageNamed:@"CharacterSheet2.png"];
    bottomBg.anchorPoint = CGPointMake(0, 0);
    bottomBg.position = CGPointMake(7, 7);
    //[bottomBg setScale:0.5];
    bottomBg.xScale = 0.5;
    bottomBg.yScale = 1.35;
    [_bottomLayer addChild:bottomBg];
    [_bgLayer addChild:_bottomLayer];
}

-(void)createFixedTopLayout {
   
    //right side
    
    SKLabelNode *charName = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    charName.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    charName.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    charName.fontColor = [SKColor blackColor];
    charName.fontSize = 35;
    charName.position = CGPointMake((_quarterWidth * 3), self.size.height - 35);
    charName.text = self.player.name;
    [_topLayer addChild:charName];
    
    _charLevel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    _charLevel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _charLevel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    _charLevel.fontColor = [SKColor blackColor];
    _charLevel.fontSize = 24;
    _charLevel.position = CGPointMake((_quarterWidth * 3), self.size.height - 65);
    NSString *lvlStr = [NSString stringWithFormat:@"Level: %i", self.player.level];
    _charLevel.text = lvlStr;
    [_topLayer addChild:_charLevel];
    
    SKLabelNode *charXpLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    charXpLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    charXpLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    charXpLabel.fontColor = [SKColor blackColor];
    charXpLabel.fontSize = 18;
    charXpLabel.position = CGPointMake((_quarterWidth * 3), self.size.height - 100);
    charXpLabel.text = @"XP:";
    [_topLayer addChild:charXpLabel];
    
    
    _charXpValue = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    _charXpValue.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _charXpValue.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    _charXpValue.fontColor = [SKColor blackColor];
    _charXpValue.fontSize = 15;
    _charXpValue.position = CGPointMake((_quarterWidth * 3), self.size.height - 125);
    NSString *xpVal = [NSString stringWithFormat:@"%i/%i", self.player.exp, [self.player nextLevelup]];
    _charXpValue.text = xpVal;
    [_topLayer addChild:_charXpValue];
    
    
    _charPick = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    _charPick.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _charPick.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    _charPick.fontColor = [SKColor blackColor];
    _charPick.fontSize = 18;
    _charPick.position = CGPointMake((_quarterWidth * 3), self.size.height - 150);
    NSString *pickStr = [NSString stringWithFormat:@"Pick: %i/%i", self.player.picks, self.player.pickLimit];
    _charPick.text = pickStr;
    [_topLayer addChild:_charPick];
    
    
    
    //left side
    
    SKLabelNode *qLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    qLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    qLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    qLabel.fontColor = [SKColor blackColor];
    qLabel.fontSize = 22;
    qLabel.position = CGPointMake(20, self.size.height - 35);
    qLabel.text = @"Quests";
    [_topLayer addChild:qLabel];
    
    SKLabelNode *easyq = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    easyq.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    easyq.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    easyq.fontColor = [SKColor blackColor];
    easyq.fontSize = 18;
    easyq.position = CGPointMake(20, self.size.height - 65);
    NSString *easyStr = [NSString stringWithFormat:@"Easy: %i", self.player.easyQuestsDone];
    easyq.text = easyStr;
    [_topLayer addChild:easyq];
    
    SKLabelNode *normalq = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    normalq.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    normalq.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    normalq.fontColor = [SKColor blackColor];
    normalq.fontSize = 18;
    normalq.position = CGPointMake(20, self.size.height - 95);
    NSString *normStr = [NSString stringWithFormat:@"Normal: %i", self.player.normalQuestsDone];
    normalq.text = normStr;
    [_topLayer addChild:normalq];
    
    SKLabelNode *hardq = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    hardq.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    hardq.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    hardq.fontColor = [SKColor blackColor];
    hardq.fontSize = 18;
    hardq.position = CGPointMake(20, self.size.height - 125);
    NSString *hardStr = [NSString stringWithFormat:@"Hard: %i", self.player.hardQuestsDone];
    hardq.text = hardStr;
    [_topLayer addChild:hardq];
    
    SKLabelNode *challengeq = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    challengeq.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    challengeq.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    challengeq.fontColor = [SKColor blackColor];
    challengeq.fontSize = 18;
    challengeq.position = CGPointMake(20, self.size.height - 155);
    NSString *chalStr = [NSString stringWithFormat:@"Challenge: %i", self.player.challengeQuestsDone];
    challengeq.text = chalStr;
    [_topLayer addChild:challengeq];
    
}

-(void)updateLabels {
    
    [_questsLayer removeFromParent];
    [_upgradesLayer removeFromParent];
    [_topLayer removeFromParent];
    [_bgLayer removeFromParent];
    
    [self createBackgrounds];
    [self createFixedTopLayout];
    [self createBottomUpgradeLayout];
    [self createBottomQuestsLayout];
    [_bottomLayer addChild:_upgradesLayer];
    
    
}


-(void)createBottomUpgradeLayout {
    
    _upgradesLayer = [[SKNode alloc]init];
    
    SKLabelNode *upgradeLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    upgradeLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    upgradeLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    upgradeLabel.fontColor = [SKColor blackColor];
    upgradeLabel.fontSize = 32;
    upgradeLabel.position = CGPointMake(self.size.width/2, self.size.height/2 + 70);
    upgradeLabel.text = @"Upgrades";
    [_upgradesLayer addChild:upgradeLabel];
    
    
    SKLabelNode *speedLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    speedLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    speedLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    speedLabel.fontColor = [SKColor blackColor];
    speedLabel.fontSize = 22;
    speedLabel.position = CGPointMake(20, self.size.height/2 + 10);
    NSString *speedStr = [NSString stringWithFormat:@"Speed: %i/5", self.player.speedUpgradeLevel];
    speedLabel.text = speedStr;
    [_upgradesLayer addChild:speedLabel];
    
    Button *speedButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Upgrade" andFontSize:16 andName:@"speedButton"];
    speedButton.position = CGPointMake(self.size.width - 70, self.size.height/2 + 12.5);
    speedButton.userInteractionEnabled = YES;
    speedButton.delegate = self;
    if (self.player.speedUpgradeLevel == 5) {
        speedButton.hidden = YES;
    }
    [_upgradesLayer addChild:speedButton];
    
    SKLabelNode *diffLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    diffLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    diffLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    diffLabel.fontColor = [SKColor blackColor];
    diffLabel.fontSize = 22;
    diffLabel.position = CGPointMake(20, self.size.height/2 - 30);
    NSString *diffStr = [NSString stringWithFormat:@"Difficulty: %i/5", self.player.diffUpgradeLevel];
    diffLabel.text = diffStr;
    [_upgradesLayer addChild:diffLabel];
    
    Button *diffButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Upgrade" andFontSize:16 andName:@"diffButton"];
    diffButton.position = CGPointMake(self.size.width - 70, self.size.height/2 -27.5);
    diffButton.userInteractionEnabled = YES;
    diffButton.delegate = self;
    if (self.player.diffUpgradeLevel == 5) {
        diffButton.hidden = YES;
    }
    
    [_upgradesLayer addChild:diffButton];
    
    SKLabelNode *compLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    compLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    compLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    compLabel.fontColor = [SKColor blackColor];
    compLabel.fontSize = 22;
    compLabel.position = CGPointMake(20, self.size.height/2 - 70);
    NSString *compStr = [NSString stringWithFormat:@"Complexity: %i/3", self.player.compUpgradeLevel];
    compLabel.text = compStr;
    [_upgradesLayer addChild:compLabel];
    
    Button *compButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Upgrade" andFontSize:16 andName:@"compButton"];
    compButton.position = CGPointMake(self.size.width - 70, self.size.height/2 -67.5);
    compButton.userInteractionEnabled = YES;
    compButton.delegate = self;
    if (self.player.compUpgradeLevel == 3) {
        compButton.hidden = YES;
    }
    [_upgradesLayer addChild:compButton];
    
    /*
    SKLabelNode *rootsLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    rootsLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    rootsLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    rootsLabel.fontColor = [SKColor blackColor];
    rootsLabel.fontSize = 22;
    rootsLabel.position = CGPointMake(20, self.size.height/2 - 110);
    NSString *rootStr = [NSString stringWithFormat:@"Roots: %lu/12", (unsigned long)[self.player.rootsList count]];
    rootsLabel.text = rootStr;
    [_upgradesLayer addChild:rootsLabel];
    
    
    
    Button *rootsButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Upgrade" andFontSize:16 andName:@"rootsButton"];
    rootsButton.position = CGPointMake(self.size.width - 70, self.size.height/2 -107.5);
    rootsButton.userInteractionEnabled = YES;
    rootsButton.delegate = self;
    if ([self.player.rootsList count] == 12) {
        rootsButton.hidden = YES;
    }
    [_upgradesLayer addChild:rootsButton];
    */
    
    //changing pos for now
    
    SKLabelNode *levelsLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    levelsLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    levelsLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    levelsLabel.fontColor = [SKColor blackColor];
    levelsLabel.fontSize = 22;
    levelsLabel.position = CGPointMake(20, self.size.height/2 - 110);
    NSString *levelStr = [NSString stringWithFormat:@"Levels: %lu", (unsigned long)[self.player.levelsList count]];
    levelsLabel.text = levelStr;
    [_upgradesLayer addChild:levelsLabel];
    
    Button *levelsButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Upgrade" andFontSize:16 andName:@"levelsButton"];
    levelsButton.position = CGPointMake(self.size.width - 70, self.size.height/2 -107.5);
    levelsButton.userInteractionEnabled = YES;
    levelsButton.delegate = self;
    [_upgradesLayer addChild:levelsButton];
    
    
    Button *backButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(135, 50) withText:@"Back" andFontSize:30 andName:@"backButton"];
    backButton.position = CGPointMake(90, 40);
    backButton.userInteractionEnabled = YES;
    backButton.delegate = self;
    [_upgradesLayer addChild:backButton];
    
    
    Button *questButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(135, 50) withText:@"Quests" andFontSize:30 andName:@"questButton"];
    questButton.position = CGPointMake(self.size.width - 90, 40);
    questButton.userInteractionEnabled = YES;
    questButton.delegate = self;
    [_upgradesLayer addChild:questButton];
    
}

-(void)createBottomQuestsLayout {
    
    _questsLayer = [[SKNode alloc]init];
    
    SKLabelNode *questLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    questLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    questLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    questLabel.fontColor = [SKColor blackColor];
    questLabel.fontSize = 32;
    questLabel.position = CGPointMake(self.size.width/2, self.size.height/2 + 70);
    questLabel.text = @"Quests";
    [_questsLayer addChild:questLabel];
    
    
    
    //quests
    
    
    SKLabelNode *easyLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    easyLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    easyLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    easyLabel.fontColor = [SKColor blackColor];
    easyLabel.fontSize = 22;
    easyLabel.position = CGPointMake(20, self.size.height/2 + 10);
    easyLabel.text = @"Easy";
    [_questsLayer addChild:easyLabel];
    
    Button *easyButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Start" andFontSize:16 andName:@"easyButton"];
    easyButton.position = CGPointMake(self.size.width - 70, self.size.height/2 + 12.5);
    easyButton.userInteractionEnabled = YES;
    easyButton.delegate = self;
    [_questsLayer addChild:easyButton];

    SKLabelNode *normalLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    normalLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    normalLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    normalLabel.fontColor = [SKColor blackColor];
    normalLabel.fontSize = 22;
    normalLabel.position = CGPointMake(20, self.size.height/2 - 30);
    normalLabel.text = @"Normal";
    [_questsLayer addChild:normalLabel];
    
    _normalButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Start" andFontSize:16 andName:@"normalButton"];
    _normalButton.position = CGPointMake(self.size.width - 70, self.size.height/2 -27.5);
    //this needs to be triggered
    _normalButton.userInteractionEnabled = YES;
    _normalButton.delegate = self;
    [_questsLayer addChild:_normalButton];
    
    SKLabelNode *hardLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    hardLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    hardLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    hardLabel.fontColor = [SKColor blackColor];
    hardLabel.fontSize = 22;
    hardLabel.position = CGPointMake(20, self.size.height/2 - 70);
    hardLabel.text = @"Hard";
    [_questsLayer addChild:hardLabel];
    
    _hardButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Start" andFontSize:16 andName:@"hardButton"];
    _hardButton.position = CGPointMake(self.size.width - 70, self.size.height/2 -67.5);
    //this needs to be triggered
    _hardButton.userInteractionEnabled = YES;
    _hardButton.delegate = self;
    [_questsLayer addChild:_hardButton];
    
    SKLabelNode *challengeLabel = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    challengeLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    challengeLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeLeft;
    challengeLabel.fontColor = [SKColor blackColor];
    challengeLabel.fontSize = 22;
    challengeLabel.position = CGPointMake(20, self.size.height/2 - 110);
    challengeLabel.text = @"Challenge";
    [_questsLayer addChild:challengeLabel];
    
    _challengeButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(100, 30) withText:@"Start" andFontSize:16 andName:@"challengeButton"];
    _challengeButton.position = CGPointMake(self.size.width - 70, self.size.height/2 -107.5);
    //this needs to be triggered
    _challengeButton.userInteractionEnabled = YES;
    _challengeButton.delegate = self;
    [_questsLayer addChild:_challengeButton];
    
    
    
    //bottom part
    
    Button *backButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(135, 50) withText:@"Back" andFontSize:30 andName:@"backButton"];
    backButton.position = CGPointMake(90, 40);
    backButton.userInteractionEnabled = YES;
    backButton.delegate = self;
    [_questsLayer addChild:backButton];
    
    
    Button *upgradesButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(135, 50) withText:@"Upgrades" andFontSize:30 andName:@"upgradesButton"];
    upgradesButton.position = CGPointMake(self.size.width - 90, 40);
    upgradesButton.userInteractionEnabled = YES;
    upgradesButton.delegate = self;
    [_questsLayer addChild:upgradesButton];
    
}


-(SKNode *)createPopupWindowWithSize:(CGSize)size andPosition:(CGPoint)position forUpgrade:(NSString *)upgradeName andCost:(int)costInPicks {
    SKNode *popup = [[SKNode alloc]init];
    SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"darkGreenBg.png"];
    //bg.size = CGSizeMake(self.size.width - 50, 400);
    //bg.position = CGPointMake(self.size.width/2, self.size.height/2 - 90);
    bg.position = position;
    bg.size = size;
    [popup addChild:bg];
    
    SKLabelNode *title = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
    title.text = @"Upgrading";
    title.fontSize = 28;
    title.fontColor = [SKColor whiteColor];
    title.position = CGPointMake(self.size.width/2 , self.size.height/2 + 60);
    [popup addChild:title];
    
    
    //body text in the popup
    SKLabelNode *bodyText1 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
    bodyText1.text = [NSString stringWithFormat:@"The upgrade for %@", upgradeName];
    bodyText1.fontSize = 20;
    bodyText1.fontColor = [SKColor whiteColor];
    bodyText1.position = CGPointMake(self.size.width/2 , self.size.height/2 + 10);
    [popup addChild:bodyText1];
    
    SKLabelNode *bodyText2 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
    bodyText2.text = [NSString stringWithFormat:@"costs %i picks.", costInPicks];
    bodyText2.fontSize = 20;
    bodyText2.fontColor = [SKColor whiteColor];
    bodyText2.position = CGPointMake(self.size.width/2, self.size.height/2 -25);
    [popup addChild:bodyText2];
    
    
    //show only available buttons
    if (self.player.picks >= costInPicks) {
        SKLabelNode *bodyText3 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
        bodyText3.text = @"Are you sure";
        bodyText3.fontSize = 20;
        bodyText3.fontColor = [SKColor whiteColor];
        bodyText3.position = CGPointMake(self.size.width/2, self.size.height/2 - 70);
        [popup addChild:bodyText3];
        
        SKLabelNode *bodyText4 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
        bodyText4.text = @"you want to upgrade?";
        bodyText4.fontSize = 20;
        bodyText4.fontColor = [SKColor whiteColor];
        bodyText4.position = CGPointMake(self.size.width/2, self.size.height/2 - 95);
        [popup addChild:bodyText4];
        
        
        Button *okButton = [[Button alloc]initWithImageNamed:@"brighterBlueButtonBg.png" andText:@"Buy" andFontSize:24 andName:@"upgradeBuy" andSize:CGSizeMake(125, 50)];
        okButton.position = CGPointMake(self.size.width - 95, 55);
        okButton.userInteractionEnabled = YES;
        okButton.delegate = self;
        [popup addChild:okButton];
        
         }else {
             //change the text if can't afford the upgrade
             SKLabelNode *bodyText3 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
             bodyText3.text = @"You need more picks";
             bodyText3.fontSize = 20;
             bodyText3.fontColor = [SKColor whiteColor];
             bodyText3.position = CGPointMake(self.size.width/2, self.size.height/2 - 70);
             [popup addChild:bodyText3];
             
             SKLabelNode *bodyText4 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
             bodyText4.text = @"to buy this upgrade.";
             bodyText4.fontSize = 20;
             bodyText4.fontColor = [SKColor whiteColor];
             bodyText4.position = CGPointMake(self.size.width/2, self.size.height/2 - 95);
             [popup addChild:bodyText4];
             
         }
    //back button is on shown always
    Button * noButton = [[Button alloc]initWithColor:[SKColor greenColor] size:CGSizeMake(125, 50) withText:@"Back" andFontSize:24 andName:@"upgradeBack"];
    noButton.position = CGPointMake(95, 55);
    noButton.userInteractionEnabled = YES;
    noButton.delegate = self;
    [popup addChild: noButton];
    
    return popup;
}

    
    
@end
