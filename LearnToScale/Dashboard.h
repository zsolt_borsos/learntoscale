//
//  Dashboard.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LtSScene.h"
#import "Button.h"
#import "SoundEngine.h"
//#import "PdDispatcher.h"


@interface Dashboard : LtSScene <buttonDelegate>

- (instancetype)initWithSize:(CGSize)size;

@end
