//
//  LevelController.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 23/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LevelController.h"
#import "LevelGenerator.h"




@implementation LevelController{
    
    NSDictionary *_questList;

}

- (instancetype)init
{
    self = [super init];
    if (self) {

        NSString *path = [[NSBundle mainBundle] pathForResource:@"LevelList" ofType:@"plist"];
        _questList = [[NSDictionary alloc]initWithContentsOfFile:path];
        
    }
    return self;
}
-(Level *)getNormalLevel {
    
    NSString *levelFileName = [self getRandomNormalLevelFileName];
    //NSString *path = [[NSBundle mainBundle] pathForResource:levelFileName ofType:@"plist"];
    
    Level *level = [[Level alloc]init];
    [level loadLevelFromList:levelFileName];
    level.type = 1;
    level.speed = 72;
    
    
    return level;
    
}


-(Level *)getHardLevel {
    
    NSString *levelFileName = [self getRandomHardLevelFileName];
    //NSString *path = [[NSBundle mainBundle] pathForResource:levelFileName ofType:@"plist"];
    
    Level *level = [[Level alloc]init];
    [level loadLevelFromList:levelFileName];
    level.type = 2;
    level.speed = 85;
    
    
    return level;
    
}

-(Level *)getChallengeLevel {
    
    NSString *levelFileName = [self getRandomChallengeLevelFileName];
    //NSString *path = [[NSBundle mainBundle] pathForResource:levelFileName ofType:@"plist"];
    
    Level *level = [[Level alloc]init];
    [level loadLevelFromList:levelFileName];
    level.type = 3;
    level.speed = 100;
    
    return level;
}

//still keep separation of categories for easier customiastion for later?
-(Level *)getEasyLevel {
    
   
    NSString *levelFileName = [self getRandomNormalLevelFileName];
    //NSString *path = [[NSBundle mainBundle] pathForResource:levelFileName ofType:@"plist"];
    
    Level *level = [[Level alloc]init];
    [level loadLevelFromList:levelFileName];
    level.speed = 60;
    level.type = 0;
    
    return level;
    
}

/*
-(Level *)getPracticeLevel:(NSString *)scaleName {
    
 
    
    
    Level *level = [[Level alloc]init];
    
    level.speed = 60;
    level.type = 0;
    
    return level;
    
}
*/

-(NSString *)getRandomEasyLevelFileName {
    
    NSMutableArray *fileNames = [[NSMutableArray alloc]init];
    
    for (NSString *diff in _questList) {
        if ([[_questList objectForKey:diff] isEqualToString:@"Easy"]) {
            [fileNames addObject:diff];
        }
    }
    int randomPos = [self getRandomNumberWithMax:(int)[fileNames count]];
    return [fileNames objectAtIndex:randomPos];
    
}

-(NSString *)getRandomChallengeLevelFileName {
    
    NSMutableArray *fileNames = [[NSMutableArray alloc]init];
    
    for (NSString *diff in _questList) {
        if ([[_questList objectForKey:diff] isEqualToString:@"Challenge"]) {
            [fileNames addObject:diff];
        }
    }
    int randomPos = [self getRandomNumberWithMax:(int)[fileNames count]];
    return [fileNames objectAtIndex:randomPos];
}

-(NSString *)getRandomHardLevelFileName {
   
    NSMutableArray *fileNames = [[NSMutableArray alloc]init];
    
    for (NSString *diff in _questList) {
        if ([[_questList objectForKey:diff] isEqualToString:@"Easy"] || [[_questList objectForKey:diff] isEqualToString:@"Normal"] || [[_questList objectForKey:diff] isEqualToString:@"Hard"]) {
            [fileNames addObject:diff];
        }
    }
    int randomPos = [self getRandomNumberWithMax:(int)[fileNames count]];
    return [fileNames objectAtIndex:randomPos];
}

-(NSString *)getRandomNormalLevelFileName {
    
    NSMutableArray *fileNames = [[NSMutableArray alloc]init];
    
    for (NSString *diff in _questList) {
        if ([[_questList objectForKey:diff] isEqualToString:@"Easy"] || [[_questList objectForKey:diff] isEqualToString:@"Normal"]) {
            [fileNames addObject:diff];
        }
    }
    int randomPos = [self getRandomNumberWithMax:(int)[fileNames count]];
    return [fileNames objectAtIndex:randomPos];

}



//call this if you only want 1 category
-(NSString *)getRandomLevelFilenameInCategory:(NSString *)category {
    
    NSMutableArray *fileNames = [[NSMutableArray alloc]init];
    
    for (NSString *diff in _questList) {
        if ([[_questList objectForKey:diff] isEqualToString:category]) {
            [fileNames addObject:diff];
        }
    }
    int randomPos = [self getRandomNumberWithMax:(int)[fileNames count]];
    return [fileNames objectAtIndex:randomPos];

    
}




-(int)getRandomNumberWithMax:(int)max {
    
    return arc4random_uniform(max);
    
}

@end
