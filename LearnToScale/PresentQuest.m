//
//  PresentQuest.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 21/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "PresentQuest.h"
#import "CharacterSheet.h"
#import "StoryGamePlay.h"

@implementation PresentQuest{
    
    SKNode *_bgLayer;
    Quest *_currentQuest;
    SKLabelNode *_reqLabel, *_levelLabel, *_rewardLabel;
    int _rewardXpForQuest;
    int _rewardPicksForQuest;
    
}

- (instancetype)initWithSize:(CGSize)size andQuest:(Quest *)quest
{
    self = [super initWithSize:size];
    if (self) {
        _currentQuest = quest;
        NSLog(@"Quest: %@", _currentQuest.level.levelName);
        
        //_rewardXpForQuest = [_currentQuest getRewardXpForQuest:_currentQuest.level.type];
        _rewardXpForQuest = _currentQuest.xp;
        _rewardPicksForQuest = _currentQuest.reward;
        
        [self createBackgrounds];
    }
    return self;
}

-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"backButton"]) {
        CharacterSheet *ch = [[CharacterSheet alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsOpenHorizontalWithDuration:0.5];
        [self.view presentScene:ch transition:transition];
    }
    
    if ([name isEqualToString:@"startQuest"]) {
    
        
        StoryGamePlay *game = [[StoryGamePlay alloc]initWithSize:self.size andQuest:_currentQuest andSpeed:_currentQuest.level.speed];
        SKTransition *transition = [SKTransition doorsOpenHorizontalWithDuration:1.5];
        [self.view presentScene:game transition:transition];
        
        //NSLog(@"Current level: %@", _currentQuest.level.complexity);

    }
}


-(void)createBackgrounds {
    _bgLayer = [[SKNode alloc]init];
    //SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"CharacterSheetBg.png"];
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor:[SKColor whiteColor] size:self.size];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    [self addChild:_bgLayer];
    
    
    SKSpriteNode *bgPic = [SKSpriteNode spriteNodeWithImageNamed:@"CharacterSheet2.png"];
    bgPic.position = CGPointMake(self.size.width/2, self.size.height/2);
    //[bgPic setScale:0.5];
    bgPic.xScale = 0.5;
    bgPic.yScale = 1.8;
    [_bgLayer addChild:bgPic];
    
    SKLabelNode *topText = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
    topText.text = @"Your quest is...";
    topText.fontSize = 34;
    topText.fontColor = [SKColor blackColor];
    topText.position = CGPointMake(self.size.width/2 , self.size.height - 80);
    [_bgLayer addChild:topText];
    
    
    if ([_currentQuest.requirements count] == 2) {
        SKLabelNode *requirementText1 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];

        requirementText1.text = [[_currentQuest.requirements objectAtIndex:0] objectForKey:@"desc"];
        requirementText1.fontSize = 20;
        requirementText1.fontColor = [SKColor blackColor];
        requirementText1.position = CGPointMake(self.size.width/2 , self.size.height - 140);
        [_bgLayer addChild:requirementText1];
        
        SKLabelNode *fillerText1 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
        fillerText1.text = @"and";
        fillerText1.fontSize = 20;
        fillerText1.fontColor = [SKColor blackColor];
        fillerText1.position = CGPointMake(self.size.width/2 , self.size.height/2 + 110);
        [_bgLayer addChild:fillerText1];
        
        
        SKLabelNode *requirementText2 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
        requirementText2.text = [[_currentQuest.requirements objectAtIndex:1] objectForKey:@"desc"];
        requirementText2.fontSize = 20;
        requirementText2.fontColor = [SKColor blackColor];
        requirementText2.position = CGPointMake(self.size.width/2 , self.size.height/2 + 80);
        [_bgLayer addChild:requirementText2];
        
    }else {
        SKLabelNode *requirementText1 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
        requirementText1.text = [[_currentQuest.requirements objectAtIndex:0] objectForKey:@"desc"];
        requirementText1.fontSize = 20;
        requirementText1.fontColor = [SKColor blackColor];
        requirementText1.position = CGPointMake(self.size.width/2 , self.size.height - 140);
        [_bgLayer addChild:requirementText1];
    
    }
    
    //present reward
    
    SKLabelNode *rewardText1 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
    rewardText1.text = @"Your reward will be:";
    rewardText1.fontSize = 20;
    rewardText1.fontColor = [SKColor blackColor];
    rewardText1.position = CGPointMake(self.size.width/2 , self.size.height/2);
    [_bgLayer addChild:rewardText1];
    
    _rewardLabel = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
    _rewardLabel.text = [NSString stringWithFormat:@"%d Xp", _rewardXpForQuest];
    _rewardLabel.fontSize = 20;
    _rewardLabel.fontColor = [SKColor blackColor];
    _rewardLabel.position = CGPointMake(self.size.width/2 , self.size.height/2 - 40);
    [_bgLayer addChild:_rewardLabel];
    
    if (_rewardPicksForQuest > 0) {
        SKLabelNode *rewardText2 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
        rewardText2.text = @"and";
        rewardText2.fontSize = 20;
        rewardText2.fontColor = [SKColor blackColor];
        rewardText2.position = CGPointMake(self.size.width/2 , self.size.height/2 - 70);
        [_bgLayer addChild:rewardText2];
        
        SKLabelNode *rewardText3 = [[SKLabelNode alloc]initWithFontNamed:@"Baskerville-SemiBold"];
        rewardText3.text = [NSString stringWithFormat:@"%d picks", _rewardPicksForQuest];
        rewardText3.fontSize = 20;
        rewardText3.fontColor = [SKColor blackColor];
        rewardText3.position = CGPointMake(self.size.width/2 , self.size.height/2 - 90);
        [_bgLayer addChild:rewardText3];
    }
    
    
    
    Button *okButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(135, 50) withText:@"Start" andFontSize:30 andName:@"startQuest"];
    okButton.position = CGPointMake(self.size.width- 90, 70);
    okButton.userInteractionEnabled = YES;
    okButton.delegate = self;
    [_bgLayer addChild:okButton];
    
    
    Button *backButton = [[Button alloc]initWithColor:[SKColor blackColor] size:CGSizeMake(135, 50) withText:@"Back" andFontSize:30 andName:@"backButton"];
    backButton.position = CGPointMake(90, 70);
    backButton.userInteractionEnabled = YES;
    backButton.delegate = self;
    [_bgLayer addChild:backButton];
    
    
}




@end
