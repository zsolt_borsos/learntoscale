//
//  QuestController.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 17/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "QuestController.h"
#import "LevelController.h"

@implementation QuestController{
    
    LevelController *_levelController;
    NSDictionary *_easyQuests, *_normalQuests, *_hardQuests, *_challengeQuests;
    
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        _levelController = [[LevelController alloc]init];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"quests" ofType:@"plist"];
        self.quests = [NSDictionary dictionaryWithContentsOfFile:path];
        _easyQuests = [self.quests objectForKey:@"Easy"];
        //NSLog(@"easy quests: %@", _easyQuests.description);
        
    }
    return self;
}


-(Quest *)getEasyQuest {
    //default xp
    int xp = 50;
    
    NSMutableArray *requirements = [[NSMutableArray alloc]init];
    
    NSDictionary *selectedRequirement = [self getQuestRequirementFromCategory:_easyQuests];
    
    [requirements addObject:selectedRequirement];
    
    //select 2nd requirements sometimes (this might need tuning a bit)
    if ([self getRandomNumberWithMax:100] > 50) {
        NSDictionary *selectedRequirement2 = [self getQuestRequirementFromCategory:_easyQuests];
        [requirements addObject:selectedRequirement2];
        //double the xp for these
        xp = 100;
    }
    
    NSLog(@"reqirements: %@", requirements.description);
    
    Quest *q = [[Quest alloc]initWithRequirements:requirements forLevel:[_levelController getEasyLevel] andReward:0];
    //worth $$$xp on completion
    q.xp = xp;
    
    return q;
    
    
}


-(Quest *)getNormalQuest {
    //default xp
    int xp = 250;
    int reward = 1;
    
    NSMutableArray *requirements = [[NSMutableArray alloc]init];
    
    NSDictionary *normalReq = [self.quests objectForKey:@"Normal"];
    
    NSDictionary *selectedRequirement = [self getNormalRequirements:normalReq];
    
    [requirements addObject:selectedRequirement];
    
    //select 2nd requirements sometimes (this might need tuning a bit)
    if ([self getRandomNumberWithMax:100] > 50) {
        NSDictionary *selectedRequirement2 = [self getNormalRequirements:[self.quests objectForKey:@"Normal"]];
        [requirements addObject:selectedRequirement2];
        //double the xp for these
        reward = 2;
        xp = 400;
    }
    
    NSLog(@"reqirements: %@", requirements.description);
    
    Quest *q = [[Quest alloc]initWithRequirements:requirements forLevel:[_levelController getNormalLevel] andReward:reward];
    //worth $$$xp on completion
    q.xp = xp;
    
    return q;

}


-(Quest *)getHardQuest {
    //default xp
    int xp = 500;
    int reward = 3;
    
    NSMutableArray *requirements = [[NSMutableArray alloc]init];
    
    NSDictionary *req = [self.quests objectForKey:@"Hard"];
    
    NSDictionary *selectedRequirement = [self getHardRequirements:req];
    
    [requirements addObject:selectedRequirement];
    
    //select 2nd requirements sometimes (this might need tuning a bit)
    if ([self getRandomNumberWithMax:100] > 50) {
        NSDictionary *selectedRequirement2 = [self getHardRequirements:req];
        [requirements addObject:selectedRequirement2];
        //double the xp for these
        reward = 4;
        xp = 1000;
    }
    
    NSLog(@"reqirements: %@", requirements.description);
    
    Quest *q = [[Quest alloc]initWithRequirements:requirements forLevel:[_levelController getHardLevel] andReward:reward];
    //worth $$$xp on completion
    q.xp = xp;
    
    return q;
    
}

-(Quest *)getChallengeQuest {
    //default xp
    int xp = 1000;
    int reward = 5;
    
    NSMutableArray *requirements = [[NSMutableArray alloc]init];
    
    NSDictionary *req = [self.quests objectForKey:@"Challenge"];
    
    NSDictionary *selectedRequirement = [self getChallengeRequirements:req];
    
    [requirements addObject:selectedRequirement];
    
    //select 2nd requirements sometimes (this might need tuning a bit)
    
    //not sure if this will work -> length might be too short
    if ([self getRandomNumberWithMax:100] > 50) {
        NSDictionary *selectedRequirement2 = [self getChallengeRequirements:req];
        [requirements addObject:selectedRequirement2];
        //double the xp for these
        reward = 7;
        xp = 2000;
    }
    
    NSLog(@"reqirements: %@", requirements.description);
    
    Quest *q = [[Quest alloc]initWithRequirements:requirements forLevel:[_levelController getChallengeLevel] andReward:reward];
    //worth $$$xp on completion
    q.xp = xp;
    
    return q;
    
}


-(NSDictionary *)getChallengeRequirements:(NSDictionary *)categoryList {
    
    int randomPos = [self getRandomNumberWithMax:(int)[categoryList count]];
    
    NSDictionary *r1 = [categoryList objectForKey:@"10c3x"];
    NSDictionary *r2 = [categoryList objectForKey:@"15c2x"];
    NSDictionary *r3 = [categoryList objectForKey:@"20c"];
    NSDictionary *r4 = [categoryList objectForKey:@"25c"];
    
    switch (randomPos) {
        case 0:
            return r1;
            break;
        case 1:
            return r2;
            break;
        case 2:
            return r3;
            break;
        case 3:
            return r4;
            break;
        default:
            return r1;
            break;
    }
    
    
}

-(NSDictionary *)getHardRequirements:(NSDictionary *)categoryList {
    
    int randomPos = [self getRandomNumberWithMax:(int)[categoryList count]];
    
    NSDictionary *r1 = [categoryList objectForKey:@"5c3x"];
    NSDictionary *r2 = [categoryList objectForKey:@"10c"];
    NSDictionary *r3 = [categoryList objectForKey:@"10c2x"];
    NSDictionary *r4 = [categoryList objectForKey:@"15c"];
    
    switch (randomPos) {
        case 0:
            return r1;
            break;
        case 1:
            return r2;
            break;
        case 2:
            return r3;
            break;
        case 3:
            return r4;
            break;
        default:
            return r1;
            break;
    }

    
}

 -(NSDictionary *)getNormalRequirements:(NSDictionary *)categoryList {
 
     int randomPos = [self getRandomNumberWithMax:(int)[categoryList count]];
     
     //NSString *s = [NSString stringWithFormat:@"%d", randomPos];
     //NSLog(@"cat: %@", [categoryList objectForKey:@"3c"]);
     
    NSDictionary *r1 = [categoryList objectForKey:@"3c3x"];
    NSDictionary *r2 = [categoryList objectForKey:@"5c"];
    NSDictionary *r3 = [categoryList objectForKey:@"5c2x"];
    NSDictionary *r4 = [categoryList objectForKey:@"8c"];

    switch (randomPos) {
        case 0:
            return r1;
            break;
        case 1:
            return r2;
            break;
        case 2:
            return r3;
            break;
        case 3:
            return r4;
            break;
    default:
            return r1;
    break;
    }

 }


//this is one of the odd problems I have had with dictionaries, where I just couldn't read them out properly (nested dictionaries)
-(NSDictionary *)getQuestRequirementFromCategory:(NSDictionary *)categoryList {
    
    int randomPos = [self getRandomNumberWithMax:(int)[categoryList count]];
    
    //NSString *s = [NSString stringWithFormat:@"%d", randomPos];
    //NSLog(@"cat: %@", [categoryList objectForKey:@"3c"]);
 
    NSDictionary *r1 = [categoryList objectForKey:@"3c"];
    NSDictionary *r2 = [categoryList objectForKey:@"3c2x"];
    NSDictionary *r3 = [categoryList objectForKey:@"5c"];
    
    if (randomPos == 0) {
        return r1;
    }else if (randomPos == 1){
        return r2;
    }else{
        return r3;
    }
    
}





-(int)getRandomNumberWithMax:(int)max {
    
    return arc4random_uniform(max);
    
}

@end
