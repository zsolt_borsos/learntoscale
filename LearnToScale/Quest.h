//
//  Quest.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 19/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Level.h"



@interface Quest : NSObject

//quest description
@property NSArray *requirements;
//the generated level for the quest
@property Level *level;

@property int reward;

@property int xp;

//0 = easy, 1 = normal, 2 = hard, 3 = challenge
@property int type;

-(id)initWithRequirements:(NSArray *)requirements forLevel:(Level *)level andReward:(int)reward;

//-(Quest *)createQuest;

-(int)getRewardXpForQuest:(int)questTypeId;
-(int)getRewardPicksForQuest:(int)questTypeId;
    
    
@end
