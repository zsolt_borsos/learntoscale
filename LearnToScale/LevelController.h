//
//  LevelController.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 23/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Level.h"

@interface LevelController : NSObject

@property NSMutableArray *levelList;

-(Level *)getEasyLevel;
-(Level *)getNormalLevel;
-(Level *)getHardLevel;
-(Level *)getChallengeLevel;


@end
