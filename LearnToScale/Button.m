//
//  Button.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 07/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Button.h"



@implementation Button{
    
    SKLabelNode *_label;
}

- (instancetype)initWithColor:(UIColor *)color size:(CGSize)size withText:(NSString *)text andFontSize:(int)fontSize andName:(NSString *)name
{
    
    self = [super initWithImageNamed:@"buttonSmall.png"];
    if (self) {
        
        
        //self.color = color;
        self.size = size;
        self.name = name;
        
        [self createLabelText:text andSize:fontSize];
        
    }
    return self;
}


- (instancetype)initWithImageNamed:(NSString *)imgName andText:(NSString *)text andFontSize:(int)fontSize andName:(NSString *)buttonName andSize:(CGSize)size {
    
     self = [super initWithImageNamed:imgName];
    if (self) {
        self.size = size;
        self.name = buttonName;
        [self createLabelText:text andSize:fontSize];
    }
    return self;
}


-(void)createLabelText:(NSString *)text andSize:(int)size {
    
    _label = [SKLabelNode labelNodeWithFontNamed:@"Noteworthy-Bold"];
    _label.text = text;
    _label.fontColor = [SKColor blackColor];
    _label.fontSize = size;
    _label.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    _label.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    [self addChild:_label];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //NSLog(@"I am pressed!! %@", self.name);
    [self.delegate buttonPressedWithName:self.name];
    
}

-(void)changeTextTo:(NSString *)text {
    
    _label.text = text;
}



@end
