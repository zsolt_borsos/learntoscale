//
//  SelectDifficulty.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 08/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "SelectDifficulty.h"
#import "Dashboard.h"

@implementation SelectDifficulty{

    SKNode *_bgLayer;
    
}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        
        [self createMenuLayout];
        
    }
    return self;
}

-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"back"] == YES) {
        Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition flipVerticalWithDuration:0.5];
        [self.view presentScene:dash transition:transition];
        
    }else{
       
        self.player.lastDiff = name.intValue;
        [self.player save];
        Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:dash transition:transition];

    }
    
    
    NSLog(@"Button pressed in Select Difficulty Screen: %@", name);
}


-(void)createMenuLayout {
    
    _bgLayer = [[SKNode alloc]init];
    
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor: [SKColor whiteColor] size:CGSizeMake(self.size.width - 20, self.size.height - 20)];
    background.position = CGPointMake(self.size.width/2, self.size.height/2);
    [_bgLayer addChild:background];
    
    
    SKLabelNode *displayScaleName = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    displayScaleName.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    displayScaleName.fontColor = [SKColor blackColor];
    displayScaleName.fontSize = 30;
    displayScaleName.position = CGPointMake(self.size.width/2, self.size.height - 50);
    displayScaleName.text = @"Difficulty";
    [_bgLayer addChild:displayScaleName];
    
    
    /*
    Button *cb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(200, 50) withText:@"Select" andFontSize:35 andName:@"select"];
    cb.position = CGPointMake(self.size.width/2, 100);
    cb.userInteractionEnabled = YES;
    cb.delegate = self;
    [_bgLayer addChild:cb];
     
     */
    
    int rowPos = self.size.height - 100;
    
    for (int i = 1; i < self.player.diffUpgradeLevel+1; i++) {
        
        NSString *diffStr = [NSString stringWithFormat:@"%i", i];
        Button *diffButton = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(240, 50) withText: diffStr andFontSize:24 andName: diffStr];
        
        diffButton.position = CGPointMake(self.size.width/2, rowPos);
        
        diffButton.userInteractionEnabled = YES;
        
        diffButton.delegate = self;
        
        [_bgLayer addChild:diffButton];
        
        rowPos -= 60;
    }
    
    
    
    Button *backb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(100, 50) withText:@" <--Back" andFontSize:20 andName:@"back"];
    backb.position = CGPointMake(65, 40);
    backb.userInteractionEnabled = YES;
    backb.delegate = self;
    [_bgLayer addChild:backb];
    
    
    [self addChild: _bgLayer];
    
    
    
    
}




@end
