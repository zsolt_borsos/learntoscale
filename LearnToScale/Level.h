//
//  Level.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 23/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Level : NSObject

//the notes that
@property NSMutableArray *notes;
//1-5
@property int difficulty;
@property NSString *levelName;
//not sure if we need this anymore ?
//@property NSArray *scaleNotes;
@property int complexity;

//amount of notes in level
@property int scaleLength;
// 0 = C, 1 = C#, 2 = D etc...
@property int root;
//multiplier for speed -->>this needs regulating overall@@
@property int speed;

//type of quest
// 0 = easy, 1 = normal, 2 = hard, 3 = challenge
@property int type;

-(void)loadLevelFromList:(NSString *)fileName;




@end
