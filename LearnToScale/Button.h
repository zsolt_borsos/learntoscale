//
//  Button.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 07/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@protocol buttonDelegate <NSObject>

-(void)buttonPressedWithName:(NSString *)name;

@end


@interface Button : SKSpriteNode

@property (nonatomic, weak)id<buttonDelegate>delegate;

- (instancetype)initWithColor:(UIColor *)color size:(CGSize)size withText:(NSString *)text andFontSize:(int)fontSize andName:(NSString *)name;

- (instancetype)initWithImageNamed:(NSString *)imgName andText:(NSString *)text andFontSize:(int)fontSize andName:(NSString *)buttonName andSize:(CGSize)size;

-(void)changeTextTo:(NSString *)text;

@end
