//
//  SplashScreen.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "SplashScreen.h"

@implementation SplashScreen{
    SKAction *_waitForAbit;
    BOOL _decided;
    
}


- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        _waitForAbit = [SKAction waitForDuration:2];
        _decided = NO;
        [self createSplashScreen];
        
    }
    return self;
}

-(void)update:(NSTimeInterval)currentTime {
    
    if (_decided == NO) {
        if (self.player.name == nil) {
            NSLog(@"Creating user.");
            [self createUser];
            
        }else{
            NSLog(@"Found user. Loading menu.");
            [self loadMainMenu];
            
        }
        _decided = YES;
    }
    
    
}


-(void)createSplashScreen {
    
    SKSpriteNode *bg = [[SKSpriteNode alloc]initWithImageNamed:@"menubg.png"];
    bg.position = CGPointMake(self.size.width/2, self.size.height/2);
    [self addChild:bg];
    
}

-(void)loadMainMenu {
    
    
    SKAction *block = [SKAction runBlock:^{
        MainMenu *scene = [[MainMenu alloc]initWithSize:self.size];
        //scene.player = self.player;
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
    }];
    [self runAction:
     [SKAction sequence:@[
                          _waitForAbit,
                          block
                          ]
      ]];
    
    
    
}



-(void)createUser {
    
    SKAction *block = [SKAction runBlock:^{
        CreateUser *scene =
        [[CreateUser alloc]initWithSize:self.size];
        //scene.player = self.player;
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:scene transition:reveal];
    }];
    [self runAction:
     [SKAction sequence:@[
                          _waitForAbit,
                          block
                          ]
      ]];
}


@end
