//
//  SelectScale.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 07/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "LtSScene.h"
#import "Button.h"

@interface SelectScale : LtSScene <buttonDelegate>

- (instancetype)initWithSize:(CGSize)size;

@end
