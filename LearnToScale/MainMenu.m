//
//  MainMenu.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 06/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "MainMenu.h"
#import "CreateUser.h"
#import "Dashboard.h"
#import "CharacterSheet.h"
#import "Options.h"

@implementation MainMenu{
    
    SKNode *_bgLayer;
}



- (instancetype)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    if (self) {
        [self createMenuLayout];
        [UIApplication sharedApplication].idleTimerDisabled = YES; 
        
    }
    return self;
}

-(void)buttonPressedWithName:(NSString *)name {
    
    if ([name isEqualToString:@"practice"] == YES) {
        //do stuff
        Dashboard *dash = [[Dashboard alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition flipVerticalWithDuration:0.5];
        [self.view presentScene:dash transition:transition];
    }
    
    if ([name isEqualToString:@"continue"] == YES) {
    
        CharacterSheet *ch = [[CharacterSheet alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorsOpenHorizontalWithDuration:0.5];
        [self.view presentScene:ch transition:transition];
        
    }
    
    if ([name isEqualToString:@"options"] == YES) {
        Options *options= [[Options alloc]initWithSize:self.size];
        SKTransition *transition = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:options transition:transition];
    }
    
    if ([name isEqualToString:@"Highscores"] == YES) {
        //do stuff
    }
    
    NSLog(@"Button pressed in Main Menu: %@", name);
}


-(void)createMenuLayout {
    
    _bgLayer = [[SKNode alloc]init];
    
    //SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor: [SKColor lightGrayColor] size:CGSizeMake(250, 300)];
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"CharSheetBottomBg.png"];
    //background.size = CGSizeMake(250, 400);
    //[background setScale: 0.5];
    background.xScale = 0.5;
    background.yScale = 0.9;
    background.position = CGPointMake(self.size.width/2, self.size.height / 2 + 130);
    [_bgLayer addChild:background];
    
    SKLabelNode *displayName = [[SKLabelNode alloc]initWithFontNamed:@"Noteworthy-Bold"];
    displayName.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    displayName.position = CGPointMake(self.size.width/2, self.size.height - 50);
    NSString *nameStr = [NSString stringWithFormat:@"Hello %@", self.player.name];
    displayName.text = nameStr;
    [_bgLayer addChild:displayName];
    
    
    Button *contButton = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(220, 50) withText:@"Continue" andFontSize:35 andName:@"continue"];
    contButton.position = CGPointMake(self.size.width/2, self.size.height/2 + 70);
    contButton.userInteractionEnabled = YES;
    contButton.delegate = self;
    [_bgLayer addChild:contButton];
  
    
    
    Button *cb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(220, 50) withText:@"Practice" andFontSize:35 andName:@"practice"];
    cb.position = CGPointMake(self.size.width/2, self.size.height/2 + 15);
    cb.userInteractionEnabled = YES;
    cb.delegate = self;
    [_bgLayer addChild:cb];

    
    Button *ob = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(220, 50) withText:@"Options" andFontSize:35 andName:@"options"];
    ob.position = CGPointMake(self.size.width/2, self.size.height/2 - 40);
    ob.userInteractionEnabled = YES;
    ob.delegate = self;
    [_bgLayer addChild:ob];
    
    
    /*
    Button *sb = [[Button alloc]initWithColor:[SKColor darkGrayColor] size:CGSizeMake(220, 50) withText:@"Scoreboard" andFontSize:35 andName:@"score"];
    sb.position = CGPointMake(self.size.width/2, self.size.height/2 - 40);
    sb.userInteractionEnabled = YES;
    sb.delegate = self;
    [_bgLayer addChild:sb];
    */
    
    [self addChild: _bgLayer];
    

    
    
}







@end
