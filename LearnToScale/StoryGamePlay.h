//
//  StoryGamePlay.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 19/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Gameplay.h"
#import "Quest.h"


@interface StoryGamePlay : Gameplay



-(id)initWithSize:(CGSize)size andQuest:(Quest *)quest andSpeed:(float)speed;

@end
