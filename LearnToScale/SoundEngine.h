//
//  SoundEngine.h
//  LearnToScale
//
//  Created by Zsolt Borsos on 10/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PdDispatcher.h"


@interface SoundEngine : NSObject <PdListener> {
    PdDispatcher *dispatcher;
    void *patch;

}

@property (nonatomic, retain)NSString *currentPitch;
@property float currentPitchMidiNumber;
@property float currentPrecisePitch;
@property BOOL loaded;
@property BOOL bgPlaying;
@property BOOL soundOn;

+(id)sharedSound;
-(void)setMyDelegateTo:(PdDispatcher *)delegate;

-(void)soundTrigger;

-(void)playBuySound;
-(void)playPageFoldingSound;
-(void)playLevelUpSound;
-(void)triggerBgMusic;
-(void)stopBgMusic;
-(void)playEasyDrums;
-(void)stopEasyDrums;


@end
