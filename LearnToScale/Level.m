//
//  Level.m
//  LearnToScale
//
//  Created by Zsolt Borsos on 23/02/2015.
//  Copyright (c) 2015 ___ZsoltBorsos___. All rights reserved.
//

#import "Level.h"
#import "Note.h"

@implementation Level{
    
    NSArray *_notesFromFile;
    
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.notes = [[NSMutableArray alloc]init];
        _difficulty = 1;
        _complexity = 1;
        //_speed = 72;
    }
    return self;
}

//reads in the plist and loads the level in _notesFromFile array
-(void)loadLevelFromList:(NSString *)fileName {
    
    
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    NSDictionary *levelDetails = [NSDictionary dictionaryWithContentsOfFile:path];
    
    self.speed = [[levelDetails objectForKey:@"Speed"] intValue];
    self.difficulty = [[levelDetails objectForKey:@"Difficulty"] intValue];
    self.levelName =  [levelDetails objectForKey:@"LevelName"];
    NSLog(@"Level name: %@", self.levelName);
    //these needs interpreting
    _notesFromFile = [levelDetails objectForKey:@"Notes"];
    self.scaleLength = (int)[_notesFromFile count];
    [self createNotesArray:_notesFromFile];
    
    
}


-(void)createNotesArray:(NSArray *)notesFromFile{
    
    for (NSArray *n in notesFromFile) {
        Note *note = [[Note alloc]initWithString:[[n objectAtIndex:0]intValue] andNumberOn:[[n objectAtIndex:1] intValue] andPosition:[[n objectAtIndex:2] intValue]];
        //note.midiNumber = [self getMidiNumberForFretNumber:note.fret onString:note.stringNumber];
        [self.notes addObject:note];
    }
}



@end
