# README #

# LearnToScale #

A game that aims to help users to practice playing scales on the guitar. The game engine uses the [PD](https://puredata.info/) library.
This game was originally developed for the Multimedia Games Programming Module at UoC.


# How to play #

For best experience: Use a splitter cable that separates mic and headphones on the iPhone so you can plug them in individually. This allows you to connect headphones/speakers and your electric guitar to the phone. 

* Required hardware: 3.5mm 4 Position to 2x 3 Position 3.5mm Headset 2-Way Splitter cable

You can play with acoustic guitar as well. Actually, you can play with anything as long as you generate some sort of pitch that is loud enough for the built in mic on your device.

Make sure you don't turn the sound on in the game though if you don't have the Y connector plugged in as it could damage the speaker in your device.

# How to install #

You need to use cocoapods for this project.

In terminal:

If you don't have cocoapods:
Install [cocoapods](http://cocoapods.org/)

* $ sudo gem install cocoapods

Go to your development folder, or wherever you want the source code to be kept. Cloning the repo by default will create the folder: learntoscale.

Clone repo.

* $ git clone <linktorepo>

Install dependencies.

* $ cd learntoscale
* $ pod install

Open the workspace file:

*  $ open LearnToScale.xcworkspace

# Important: Always open the project with the Workspace file. #

The game should run on the simulator as well (iPhone 5+).